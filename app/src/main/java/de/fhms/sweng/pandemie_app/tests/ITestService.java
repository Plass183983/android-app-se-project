//Jannik Ernst

package de.fhms.sweng.pandemie_app.tests;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PUT;
import retrofit2.http.Path;

//Jannik Ernst
public interface ITestService {

    @GET("rest/teststationen")
    public Call<List<TeststationenTO>> listAllTeststationen(@Header("Authorization") String Jwt);

    @GET("rest/termine/freigeschaltet/{id}")
    public Call<List<TermineTO>> getAvailableTermine(@Path("id") int id, @Header("Authorization") String Jwt);

    @GET("rest/teststationen/{id}")
    public Call<TeststationenTO> getTeststationById(@Path("id") int id, @Header("Authorization") String Jwt);

    @PUT("rest/termine/{stationId}/{terminId}")
    public Call<Void> terminBuchen(@Path("stationId") int stationId, @Path("terminId") int terminId, @Header("Authorization") String Jwt);

    @PUT("rest/termine/freischalten/{stationId}/{terminId}")
    public Call<Void> terminFreischalten(@Path("stationId") int stationId, @Path("terminId") int terminId, @Header("Authorization") String Jwt);

    @GET("rest/termine/{stationId}")
    public Call<List<TermineTO>> getNichtFreigeschalteteTermine(@Path("stationId") int stationId, @Header("Authorization") String Jwt);

    @GET("rest/termine/user")
    public Call<List<TermineTO>> getTermineforUser(@Header("Authorization") String Jwt);

    @GET("rest/termine/user/{user}")
    public Call<List<TermineTO>> getTermineforUserTestende(@Path("user") String user, @Header("Authorization") String Jwt);

    @PUT("rest/termine/{terminId}")
    public Call<Void> terminStornieren(@Path("terminId") int terminId, @Header("Authorization") String Jwt);

    @PUT("rest/teststationen/{name}/{descr}/{cap}/{street}/{no}/{zip}/{city}")
    public Call<TeststationenTO> createNewTeststation(@Path("name") String name, @Path("descr") String descr, @Path("cap") int cap, @Path("street") String street, @Path("no") String no, @Path("zip") int zip, @Path("city") String city, @Header("Authorization") String Jwt);

    @DELETE("rest/teststationen/{id}")
    public Call<Void> deleteTeststation(@Path("id") int id, @Header("Authorization") String Jwt);

    @GET("rest/termine/terminbyid/{terminId}")
    public Call<TermineTO> getTerminById(@Path("terminId") int terminId, @Header("Authorization") String Jwt);

    @PUT("rest/termine/tests/{terminId}/{notizen}/{status}")
    public Call<TermineTO> createNewTest (@Path("terminId") int terminId, @Path("notizen") String notizen, @Path("status") StatusTO status, @Header("Authorization") String Jwt);

    @GET("rest/termine/terminewithtest")
    public Call<List<TermineTO>> getTermineWithTestsforUser(@Header("Authorization") String Jwt);

    @PUT("rest/teststationen/{id}/{name}/{beschreibung}/{kapazitaet}/{strasse}/{hausnr}/{plz}/{city}")
    public Call<TeststationenTO> updateTeststationen(@Path("id") int id, @Path("name") String name, @Path("beschreibung") String beschreibung, @Path("kapazitaet") int kapazitaet, @Path("strasse") String strasse, @Path("hausnr") String hausnr, @Path("plz") int plz, @Path("city") String city, @Header("Authorization") String Jwt);
}
