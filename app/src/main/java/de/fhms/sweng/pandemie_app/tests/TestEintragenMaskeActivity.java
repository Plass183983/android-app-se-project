package de.fhms.sweng.pandemie_app.tests;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//Jannik Ernst
public class TestEintragenMaskeActivity extends AppCompatActivity {

    TextView datum;
    TextView uhrzeit;
    RadioButton positiv;
    RadioButton negativ;
    RadioButton inArbeit;
    EditText notizen;
    Button button;
    StatusTO status;
    BuildConnection connect = new BuildConnection();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_eintragen_maske);
        datum = findViewById(R.id.textView17);
        uhrzeit = findViewById(R.id.textView18);
        positiv = findViewById(R.id.radioButton2);
        negativ = findViewById(R.id.radioButton3);
        inArbeit = findViewById(R.id.radioButton4);
        button = findViewById(R.id.button6);
        notizen = findViewById(R.id.editTextTextMultiLine);

        final PandemicApp pandemicApp = (PandemicApp) getApplication();
        Call<TermineTO> einsetzen = connect.getService().getTerminById(getIntent().getIntExtra("id", 0), "Bearer " + pandemicApp.getJwt());
        einsetzen.enqueue(new Callback<TermineTO>() {
            @Override
            public void onResponse(Call<TermineTO> call, Response<TermineTO> response) {
                TermineTO termine = response.body();
                String test = termine.getDate();
                datum.append(termine.getDate());
                uhrzeit.append(termine.getUhrzeit());
            }

            @Override
            public void onFailure(Call<TermineTO> call, Throwable t) {

            }
        });

        positiv.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = StatusTO.POSITIV;
            }
        }));

        negativ.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = StatusTO.NEGATIV;
            }
        }));

        inArbeit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                status = StatusTO.INARBEIT;
            }
        });

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PandemicApp pandemicApp = (PandemicApp) getApplication();
                Call<TermineTO> senden = connect.getService().createNewTest(getIntent().getIntExtra("id", 0), notizen.getText().toString(), status, "Bearer " + pandemicApp.getJwt());
                senden.enqueue(new Callback<TermineTO>() {
                    @Override
                    public void onResponse(Call<TermineTO> call, Response<TermineTO> response) {
                        AlertDialog.Builder msg = new AlertDialog.Builder(TestEintragenMaskeActivity.this);
                        msg.setTitle("Meldung");
                        msg.setMessage("Test erfolgreich eingetragen!");
                        msg.setCancelable(false);
                        msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                Intent i = new Intent(TestEintragenMaskeActivity.this, TestEintragenActivity.class);
                                finish();
                                startActivity(i);

                            }
                        });
                        msg.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        });
                        AlertDialog msgDialog = msg.create();
                        msgDialog.show();
                    }

                    @Override
                    public void onFailure(Call<TermineTO> call, Throwable t) {

                    }
                });
            }
        });

    }
}