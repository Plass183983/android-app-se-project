package de.fhms.sweng.pandemie_app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import de.fhms.sweng.pandemie_app.Kontaktverfolgung.KontaktverfolgungMenuActivity;
import de.fhms.sweng.pandemie_app.VaxService.MainVaxStationActivity;
import de.fhms.sweng.pandemie_app.tests.Untermenue;

public class MainActivity extends AppCompatActivity {

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button1;
        Button button2;
        Button button3;
        button1 = findViewById(R.id.button10);
        button2 = findViewById(R.id.button11);
        button3 = findViewById(R.id.button12);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Untermenue.class); //Button "VaxService": Einfach Activity einfügen!
                startActivity(intent);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, KontaktverfolgungMenuActivity.class); //Button "Teststationen": Einfach Activity einfügen!
                startActivity(intent);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, MainVaxStationActivity.class); //Button "Verwaltung": Einfach Activity einfügen!
                startActivity(intent);
            }
        });

    }

    public void navigateTo(View button){
        Button btn = (Button) button;
        String tag = (String) btn.getTag();

        try {
            Intent i= new Intent(this, Class.forName(tag));
            startActivity(i);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}