package de.fhms.sweng.pandemie_app.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.user.data.UserAddressTo;
import de.fhms.sweng.pandemie_app.user.data.UserDataTo;
import de.fhms.sweng.pandemie_app.user.data.inbound.AuthResponseTo;
import de.fhms.sweng.pandemie_app.user.service.IUserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

// Niklas Plaß
public class RegisterActivity extends AppCompatActivity {
    TextView viewFirstName;
    TextView viewLastName;
    TextView viewPhone;
    TextView viewEmail;
    TextView viewPassword;
    TextView viewPasswordRepeat;
    TextView viewCountry;
    TextView viewPostal;
    TextView viewCity;
    TextView viewStreet;
    TextView errorField;
    Button submit;
    ProgressBar progressBar;
    PandemicApp app;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_register);
        this.app = (PandemicApp) getApplication();

        this.viewFirstName = findViewById(R.id.userFirstName);
        this.viewLastName = findViewById(R.id.userLastName);
        this.viewEmail = findViewById(R.id.userCreateEmail);
        this.viewPhone = findViewById(R.id.userPhone);
        this.viewCity = findViewById(R.id.userCity);
        this.viewPostal = findViewById(R.id.userPostal);
        this.viewCountry = findViewById(R.id.userCountry);
        this.viewStreet = findViewById(R.id.userStreet);
        this.viewPassword = findViewById(R.id.userCreatePassword);
        this.viewPasswordRepeat = findViewById(R.id.userCreatePasswordRepeat);
        //Submit
        this.submit = findViewById(R.id.registerUserBtn);

        //Additional
        this.errorField = findViewById(R.id.registerError);
        this.progressBar = findViewById(R.id.progressBarRegister);
        this.progressBar.setVisibility(View.GONE);

        //Listener
        this.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               //TODO: Submit logic implementieren
                submitData();
            }
        });
    }

    protected void submitData() {
        if (this.validData()) {
            UserAddressTo userAddress = new UserAddressTo(
                    this.viewCountry.getText().toString(),
                    this.viewPostal.getText().toString(),
                    this.viewCity.getText().toString(),
                    this.viewStreet.getText().toString()
            );

            UserDataTo userData = new UserDataTo(
                    this.viewFirstName.getText().toString(),
                    this.viewLastName.getText().toString(),
                    this.viewEmail.getText().toString(),
                    this.viewPhone.getText().toString(),
                    this.viewPassword.getText().toString(),
                    this.viewPasswordRepeat.getText().toString(),
                    userAddress
            );

            this.sendData(userData);
        }
    }

    protected boolean validData() {
        this.errorField.setText("");

        if(!this.viewPassword.getText().toString().equals(this.viewPasswordRepeat.getText().toString())
            || this.viewPassword.getText().toString().equals("")) {
            this.errorField.setText("Passwörter müssen übereinstimmen!");
            return false;
        }
        return true;
    }

    protected void sendData(UserDataTo userData) {
        errorField.setText("");
        this.progressBar.setVisibility(View.VISIBLE);

        IUserService userService = RetroFit.init();
        final PandemicApp pandemicApp = (PandemicApp) getApplication();

        Call<AuthResponseTo> call = userService.register(userData);
        call.enqueue( new Callback<AuthResponseTo>() {
            @Override
            public void onResponse(Call<AuthResponseTo> call, Response<AuthResponseTo> response) {
                progressBar.setVisibility(View.GONE);

                if (response.isSuccessful() && response.body() != null) {
                    AuthResponseTo res = response.body();
                    pandemicApp.setJwt(res.getJwt());
                    pandemicApp.setUserRole(res.getRole());

                    //Redirect to homepage
                    Intent intent = new Intent(getApplicationContext(), UserMainActivity.class);
                    startActivity(intent);
                } else {
                    if(response.code() == 409) {
                        errorField.setText("Error: Email wird bereits verwendet");
                    } else {
                        errorField.setText("Error: Bitte fülle alle Felder korrekt aus!");
                    }
                }
            }

            @Override
            public void onFailure(Call<AuthResponseTo> call, Throwable t) {
                System.out.println("Communication error occured");
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
