package de.fhms.sweng.pandemie_app.tests;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//Jannik Ernst
public class TestEintragenActivity extends AppCompatActivity {

    private ListView listView;
    private EditText editText;
    private Button senden;
    private BuildConnection connect = new BuildConnection();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_eintragen);
        listView = findViewById(R.id.listView10);
        editText = findViewById(R.id.editTextTextPersonName2);
        senden = findViewById(R.id.button8);

        ITestService terminService = connect.getService();

        senden.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PandemicApp pandemicApp = (PandemicApp) getApplication();
                Call<List<TermineTO>> call = terminService.getTermineforUserTestende(editText.getText().toString(), "Bearer " + pandemicApp.getJwt());

                call.enqueue(new Callback<List<TermineTO>>() {
                    @Override
                    public void onResponse(Call<List<TermineTO>> call, Response<List<TermineTO>> response) {
                        if (!response.isSuccessful()) {
                            if (response.code() == 403) {
                                AlertDialog.Builder msg = new AlertDialog.Builder(TestEintragenActivity.this);
                                msg.setTitle("Meldung");
                                msg.setMessage("Sie haben nicht die erforderlichen Berechtigungen!");
                                msg.setCancelable(false);
                                msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });
                                AlertDialog msgDialog = msg.create();
                                msgDialog.show();
                            }
                            else if (response.code() == 404) {
                                AlertDialog.Builder msg = new AlertDialog.Builder(TestEintragenActivity.this);
                                msg.setTitle("Meldung");
                                msg.setMessage("Keine gebuchten Termine für diesen Nutzer vorhanden.");
                                msg.setCancelable(false);
                                msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });
                                AlertDialog msgDialog = msg.create();
                                msgDialog.show();
                            }
                            else {
                                return;
                            }
                        }
                        else {
                            List<TermineTO> termine = response.body();
                            List<String> daten = new ArrayList<>();
                            Map<String, Integer> map = new HashMap<>();

                            for (TermineTO a : termine) {
                                daten.add("Datum: " + a.getDate() + " um " + a.getUhrzeit() + " Uhr");
                                map.put("Datum: " + a.getDate() + " um " + a.getUhrzeit() + " Uhr", a.getId());
                            }

                            ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(TestEintragenActivity.this, android.R.layout.simple_list_item_1, daten);
                            listView.setAdapter(arrayAdapter);
                            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                    String tmp = (String) listView.getItemAtPosition(position);
                                    Intent i = new Intent(TestEintragenActivity.this, TestEintragenMaskeActivity.class);
                                    i.putExtra("id", map.get(tmp));
                                    finish();
                                    startActivity(i);
                                }
                            });
                        }
                    }

                    @Override
                    public void onFailure(Call<List<TermineTO>> call, Throwable t) {

                    }
                });

            }
        });

    }
}