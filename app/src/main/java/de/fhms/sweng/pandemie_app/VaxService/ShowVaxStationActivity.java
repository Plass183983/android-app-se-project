//Autor - Linus Schwinhorst
package de.fhms.sweng.pandemie_app.VaxService;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.VaxService.data.UserDTO;
import de.fhms.sweng.pandemie_app.VaxService.data.VaxStationDTO;
import de.fhms.sweng.pandemie_app.VaxService.service.IVaxService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowVaxStationActivity extends AppCompatActivity {
    private IVaxService vaxService;
    private ListView vaxStationList;
    private PandemicApp pandemicApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_vax_station);
        this.vaxStationList = findViewById(R.id.vaxStationList);

            this.pandemicApp = (PandemicApp) getApplication();
            this.vaxService = RetroFitConnection.init();

            //Show all VaxStations on create
            Call<List<VaxStationDTO>> vaxProofDTOCall = this.vaxService.getNearestVaxStation("Bearer " + this.pandemicApp.getJwt());
            vaxProofDTOCall.enqueue(new Callback<List<VaxStationDTO>>() {
                @Override
                public void onResponse(Call<List<VaxStationDTO>> listCall, Response<List<VaxStationDTO>> response) {
                    AlertDialog.Builder msg = new AlertDialog.Builder(ShowVaxStationActivity.this);
                    if (response.isSuccessful() && response.body() != null) {
                        msg.setMessage("Sie haben die Impfstationen angefordert.");
                    } else {
                        msg.setMessage("Anfordern der Impfstationen fehlgeschlagen.");
                    }

                    List<VaxStationDTO> vaxStations = response.body();
                    List<String> data = new ArrayList<>();
                    for(VaxStationDTO vd : vaxStations) {
                        data.add("Impfstation" + " " + vd.getName() + " " + "in der Adresse:" + " " + vd.getAddress());
                    }

                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(ShowVaxStationActivity.this, android.R.layout.simple_list_item_1, data);
                    vaxStationList.setAdapter(arrayAdapter);
                    vaxStationList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            AlertDialog.Builder alert = new AlertDialog.Builder(ShowVaxStationActivity.this);
                            alert.setMessage("Impfstation hinzufügen");
                            alert.setPositiveButton("Bestätigen", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //final PandemicApp pandemicApp = (PandemicApp) getApplication();
                                    //String itemAtPosition = (String) vaxStationList.getItemAtPosition(position);
                                    Call<UserDTO> userDTOCall = vaxService.getUserById("Bearer " + pandemicApp.getJwt(), 1);
                                    vaxService.setVaxStationForUser("Bearer " + pandemicApp.getJwt(), "annamailde", 1);
                                    alert.setTitle("Erfolg!");
                                    alert.setMessage("Impfstation hinzugefügt");
                                }
                            });
                        }
                    });
                }
                @Override
                public void onFailure(Call<List<VaxStationDTO>> listCall, Throwable t) {
                    System.out.println("A connection Error occured");
                }
            });
    }

}