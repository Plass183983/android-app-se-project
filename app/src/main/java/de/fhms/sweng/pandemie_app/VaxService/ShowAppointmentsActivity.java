//Autor - Linus Schwinhorst
package de.fhms.sweng.pandemie_app.VaxService;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.VaxService.data.AppointmentDTO;
import de.fhms.sweng.pandemie_app.VaxService.data.UserDTO;
import de.fhms.sweng.pandemie_app.VaxService.data.VaxStationDTO;
import de.fhms.sweng.pandemie_app.VaxService.service.IVaxService;
import de.fhms.sweng.pandemie_app.shared.UserRole;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowAppointmentsActivity extends AppCompatActivity {
    private TextView userName;
    private IVaxService vaxService;
    private Button confirm;
    private PandemicApp pandemicApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_appointments);

        this.pandemicApp = (PandemicApp) getApplication();

        //Security checks for permissions
        if(this.pandemicApp.getJwt() == null || !this.pandemicApp.getUserRole().equals(UserRole.VACCINATE)){
            AlertDialog.Builder msg = new AlertDialog.Builder(ShowAppointmentsActivity.this);
            msg.setTitle("No Permission");
            msg.setMessage("You have no Power here!");
            Intent i= new Intent(this, MainVaxStationActivity.class);
            startActivity(i);
        }

        this.userName = findViewById(R.id.userNameTxt);
        this.confirm = findViewById(R.id.confirmBtn);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = userName.getText().toString();

                triggerRestClient(name);
            }
        });
    }

    //Gets all appointments for a VaStation
    public void triggerRestClient(String userName){
        this.vaxService = RetroFitConnection.init();

        Call<List<AppointmentDTO>> listCall = this.vaxService.getAllAppointments("Bearer " + this.pandemicApp.getJwt(), userName);

        listCall.enqueue( new Callback<List<AppointmentDTO>>() {
            @Override
            public void onResponse(Call<List<AppointmentDTO>> listCall, Response<List<AppointmentDTO>> response) {
                AlertDialog.Builder msg = new AlertDialog.Builder(ShowAppointmentsActivity.this);
                if(response.isSuccessful() && response.body() != null) {
                    msg.setMessage("Sie haben die Termine angefordert.");
                } else {
                    msg.setMessage("Anfordern der Termine fehlgeschlagen.");
                }
                List<AppointmentDTO> appointmentDTOS = response.body();
                List<String> data = new ArrayList<>();
                for(AppointmentDTO ad : appointmentDTOS) {
                    data.add("Termin am " + " " + ad.getDate());
                }
            }
            
            @Override
            public void onFailure(Call<List<AppointmentDTO>> listCall, Throwable exception) {
                System.out.println("Communication error occured");
            }
        });
    }
}