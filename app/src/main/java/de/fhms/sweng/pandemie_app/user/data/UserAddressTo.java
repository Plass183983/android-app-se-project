package de.fhms.sweng.pandemie_app.user.data;

//Niklas Plaß
public class UserAddressTo {
    private String country;
    private String postal;
    private String city;
    private String street;

    public UserAddressTo() {}

    public UserAddressTo(String country, String postal, String city, String street) {
        this.country = country;
        this.postal = postal;
        this.city = city;
        this.street = street;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getPostal() {
        return postal;
    }

    public void setPostal(String postal) {
        this.postal = postal;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }
}
