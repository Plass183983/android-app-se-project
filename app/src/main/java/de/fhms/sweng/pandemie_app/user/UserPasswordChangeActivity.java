package de.fhms.sweng.pandemie_app.user;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.user.data.UserDataTo;
import de.fhms.sweng.pandemie_app.user.data.inbound.AuthResponseTo;
import de.fhms.sweng.pandemie_app.user.service.IUserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

// Niklas Plaß
public class UserPasswordChangeActivity extends AppCompatActivity {
    private TextView passwordField;
    private TextView passwordRepeatField;
    private TextView errorMsg;
    private ImageView checkIcon;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_password_change);
        this.passwordField = findViewById(R.id.passwordEdit);
        this.passwordRepeatField = findViewById(R.id.passwordEditRepeat);
        this.errorMsg = findViewById(R.id.editPasswordError);
        this.checkIcon = findViewById(R.id.checkIcon);

        this.checkIcon.setVisibility(View.GONE);
    }

    public void changePassword() {
        this.errorMsg.setText("");
        String password = this.passwordField.getText().toString();
        String passwordRepeat = this.passwordRepeatField.getText().toString();
        UserDataTo userData = new UserDataTo();
        userData.setPassword(password);
        userData.setPasswordRepeat(passwordRepeat);

        IUserService userService = RetroFit.init();
        PandemicApp app = (PandemicApp) getApplication();

        Call<AuthResponseTo> call = userService.changeCurrentUserPassword( "Bearer " + app.getJwt(), userData);
        call.enqueue( new Callback<AuthResponseTo>() {
            @Override
            public void onResponse(Call<AuthResponseTo> call, Response<AuthResponseTo> response) {
                if (response.isSuccessful() && response.body() != null) {
                    AuthResponseTo res = response.body();
                    displayCheckIcon();
                } else {
                    if(response.code() == 409) {
                        errorMsg.setText("Achtung: Passwörter stimmen nicht überein!");
                    } else {
                        errorMsg.setText("Error: " + response.message());
                    }
                }
            }

            @Override
            public void onFailure(Call<AuthResponseTo> call, Throwable t) {
                System.out.println("Communication error occurred");
                errorMsg.setText("Communication error occurred");
            }
        });
    }

    public void changePassword(View button) {
        new AlertDialog.Builder(UserPasswordChangeActivity.this)
                .setTitle("Passwort ändern")
                .setMessage("Sicher dass das Passwort geändert werden soll?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Toast.makeText(UserPasswordChangeActivity.this, "Daten werden übermittelt...", Toast.LENGTH_SHORT).show();
                        if(passwordField.getText().length() < 7) {
                            errorMsg.setText("Passwort ist zu kurz. Bitte mindestens 8 Zeichen");
                        } else {
                            changePassword();
                        }
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    private void displayCheckIcon() {
        this.checkIcon.setVisibility(View.VISIBLE);
        this.checkIcon.postDelayed(new Runnable() {
            public void run() {
                checkIcon.setVisibility(View.GONE);
            }
        }, 1500);
    }

    public void goBack(View button) {
        Intent intent = new Intent(getApplicationContext(), UserMainActivity.class);
        startActivity(intent);
    }

}
