package de.fhms.sweng.pandemie_app.VaxService.service;

import de.fhms.sweng.pandemie_app.VaxService.data.*;

import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface IVaxService{

    //Gets nearest VaxStation. Doesn't work yet. Gets all Vaxstations
    @GET("/rest/vax/station")
    public Call<List<VaxStationDTO>> getNearestVaxStation(@Header("Authorization") String jwt);

    //Gets VaxStation by User
    @GET("/rest/vax/station/user/{id}")
    public Call<VaxStationDTO> getVaxStationByUser(@Header("Authorization") String jwt, @Path("userName") Integer id);

    //Gets Users for a Waitlist
    @GET("/rest/vax/waitlist/{id}")
    public Call<List<UserDTO>> getWaitList(@Header("Authorization") String jwt, @Path("id") Integer id);

    //Gets VaxStations by Id
    @GET("/rest/vax/station/{id}")
    public Call<VaxStationDTO> getVaxStationById(@Header("Authorization") String jwt, @Path("id") Integer id);

    //Gets User by Id
    @GET("/rest/vax/user/{id}")
    public Call<UserDTO> getUserById(@Header("Authorization") String jwt, @Path("id") Integer id);

    //Gets all appointments
    @GET("/rest/vax/appointments/{userName}")
    public Call<List<AppointmentDTO>> getAllAppointments(@Header("Authorization") String jwt, @Path("userName") String userName);

    //Gets VaxProof for User
    @GET("/rest/vax/proof/{userName}")
    public Call<VaxProofDTO> getVaxProof(@Header("Authorization") String jwt, @Path("userName") String userName);

    //Adds a Vaccination for a User
    @POST("/rest/vax/vaccinate/{name}")
    public Call<VaxProofDTO> addVaccination(@Header("Authorization") String jwt, @Path("name") String userName);

    //Sets the current VaxStations for a User
    @POST("user/{name}/{id}")
    public Call<Void> setVaxStationForUser(@Header("Authorization") String jwt, @Path("name") String name, @Path("id") Integer id);

    //Adds a VaxStation
    @POST("/rest/vax/{userName}/{name}/{address}/{phoneNumber}/{email}")
    public Call<VaxStationDTO> addVaxStation (@Header("Authorization") String jwt, @Path("userName") String userName, @Path("name") String name, @Path("address") String address, @Path("phoneNumber") String phoneNumber, @Path("email") String email);

    //Updates a VaxStation
    @PUT("/rest/vax/{id}/{userName}/{name}/{address}/{phoneNumber}/{email}")
    public Call<VaxStationDTO> editVaxStation (@Header("Authorization") String jwt, @Path("userName") String userName, @Path("id") Integer id, @Path("name") String name, @Path("address") String address, @Path("phoneNumber") String phoneNumber, @Path("email") String email);

    //Deletes a VaxStation
    @DELETE("/rest/vax/vaxStation/{id}/{userName}")
    public Call<Void> deleteVaxStation(@Header("Authorization") String jwt, @Path("id") Integer id, @Path("userName") String userName);

    //Cancels an Appointment
    @DELETE("/rest/vax/appointment/{userName}")
    public Call<Void> cancelAppointment(@Header("Authorization") String jwt, @Path("userName") String userName);

    //Enables new Appointments for a Waitlist
    @POST("/rest/vax/appointment/{userName}/{amount}/{priorityGroup}")
    public Call<WaitListDTO> enableNewAppointments (@Header("Authorization") String jwt, @Path("userName") String userName, @Path("amount") Integer amount, @Path("priorityGroup") Integer priorityGroup);

    //Adds User to a Waitlist
    @POST("/rest/vax/waitlist/{id}/{priorityGroup}")
    public Call<WaitListDTO> addToWaitlist(@Header("Authorization") String jwt, @Path("id") Integer id, @Path("priorityGroup") Integer priorityGroup);

}
