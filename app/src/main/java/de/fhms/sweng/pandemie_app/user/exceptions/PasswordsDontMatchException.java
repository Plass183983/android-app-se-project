package de.fhms.sweng.pandemie_app.user.exceptions;

//Niklas Plaß
public class PasswordsDontMatchException extends Exception{
    public PasswordsDontMatchException(String message) {
        super(message);
    }
}
