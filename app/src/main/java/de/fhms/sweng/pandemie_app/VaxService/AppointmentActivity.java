//Autor - Linus Schwinhorst
package de.fhms.sweng.pandemie_app.VaxService;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;

import de.fhms.sweng.pandemie_app.R;

public class AppointmentActivity extends AppCompatActivity {
private Button addAppointment;
private Button cancelAppointment;
private Button showAppointments;
private Button showWaitlist;

    //Navigation activity for Appointments
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_appointment);

        this.addAppointment = findViewById(R.id.addAppointmentBtn);
        this.cancelAppointment = findViewById(R.id.cancelAppointmentButton);
        this.showAppointments = findViewById(R.id.showAppointmentsBtn);
        this.showAppointments = findViewById(R.id.showWaiListBtn);
    }

    public void navigateTo(View button){
        Button btn = (Button) button;
        String tag = (String) btn.getTag();

        try {
            Intent i= new Intent(this, Class.forName(tag));
            startActivity(i);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}