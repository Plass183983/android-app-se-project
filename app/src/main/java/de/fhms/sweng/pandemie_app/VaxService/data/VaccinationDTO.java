package de.fhms.sweng.pandemie_app.VaxService.data;

import java.util.Date;

public class VaccinationDTO {
    private String vaxName;
    private Integer batchNumber;
    private Date useUntil;
    private Integer amount;

    public VaccinationDTO(){}

    public VaccinationDTO(String vaxName, Integer batchNumber, Date useUntil, Integer amount){
        this.vaxName = vaxName;
        this.batchNumber = batchNumber;
        this.useUntil = useUntil;
        this.amount = amount;
    }

    public String getVaxName() {
        return vaxName;
    }

    public void setVaxName(String vaxName) {
        this.vaxName = vaxName;
    }

    public Integer getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(Integer batchNumber) {
        this.batchNumber = batchNumber;
    }

    public Date getUseUntil() {
        return useUntil;
    }

    public void setUseUntil(Date useUntil) {
        this.useUntil = useUntil;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }
}