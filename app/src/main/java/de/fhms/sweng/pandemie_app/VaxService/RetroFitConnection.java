//Autor - Linus Schwinhorst
package de.fhms.sweng.pandemie_app.VaxService;

import de.fhms.sweng.pandemie_app.VaxService.service.IVaxService;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroFitConnection {
    private final static String VM_URL = "http://stu-fb09-015.fh-muenster.de:8080";
    //For testing
    private final static String SERVICE_URL = "http://test-sweng-sweng-team16-api-gateway.wi-k8s.fh-muenster.de/";
    //For production
    private final static String SERVICE_URL_PROD = "http://sweng-sweng-team16-api-gateway.wi-k8s.fh-muenster.de/";

    //Initiate Retrofit connection
    public static IVaxService init() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SERVICE_URL_PROD)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(IVaxService.class);
    }
}