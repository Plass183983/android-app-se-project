package de.fhms.sweng.pandemie_app.user.data.inbound;

import java.time.LocalDate;

// Niklas Plaß
public class CovidStatusTo {
    private String Country;
    private Integer Confirmed;
    private Integer Deaths;
    private Integer Recovered;
    private Integer Active;
    private String Date;
    private Integer newConfirmed;
    private Integer newDeaths;
    private Integer differenceNewConfirmed;

    public CovidStatusTo() {}

    public CovidStatusTo(String country, Integer confirmed, Integer deaths, Integer recovered, Integer active, String date, Integer newConfirmed, Integer newDeaths, Integer differenceNewConfirmed) {
        Country = country;
        Confirmed = confirmed;
        Deaths = deaths;
        Recovered = recovered;
        Active = active;
        Date = date;
        this.newConfirmed = newConfirmed;
        this.newDeaths = newDeaths;
        this.differenceNewConfirmed = differenceNewConfirmed;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }

    public Integer getConfirmed() {
        return Confirmed;
    }

    public void setConfirmed(Integer confirmed) {
        Confirmed = confirmed;
    }

    public Integer getDeaths() {
        return Deaths;
    }

    public void setDeaths(Integer deaths) {
        Deaths = deaths;
    }

    public Integer getRecovered() {
        return Recovered;
    }

    public void setRecovered(Integer recovered) {
        Recovered = recovered;
    }

    public Integer getActive() {
        return Active;
    }

    public void setActive(Integer active) {
        Active = active;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public Integer getNewConfirmed() {
        return this.newConfirmed;
    }

    public void setNewConfirmed(Integer newConfirmed) {
        this.newConfirmed = newConfirmed;
    }

    public Integer getNewDeaths() {return this.newDeaths;}

    public void setNewDeaths(Integer newDeaths) { this.newDeaths = newDeaths;}

    public Integer getDifferenceNewConfirmed() { return this.differenceNewConfirmed;}

    public void setDifferenceNewConfirmed(Integer differenceNewConfirmed) { this.differenceNewConfirmed = differenceNewConfirmed;}

    @Override
    public String toString() {
        return " { " +
                " country: " + this.getCountry() + " " +
                " confirmed: " + this.getConfirmed() + " " +
                " deaths: " + this.getDeaths() + " " +
                " active: " + this.getActive() + " " +
                " newConfirmed: " + this.getNewConfirmed() + " " +
                " recovered: " + this.getRecovered() + " " +
                " date: " + this.getDate() + " " +
                " newDeaths: " + this.getNewDeaths() + "  }";
    }
}
