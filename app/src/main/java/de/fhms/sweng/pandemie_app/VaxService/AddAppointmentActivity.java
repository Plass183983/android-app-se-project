//Autor - Linus Schwinhorst
package de.fhms.sweng.pandemie_app.VaxService;

import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import de.fhms.sweng.pandemie_app.VaxService.data.AppointmentDTO;
import de.fhms.sweng.pandemie_app.VaxService.data.WaitListDTO;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import de.fhms.sweng.pandemie_app.VaxService.service.IVaxService;

public class AddAppointmentActivity extends AppCompatActivity {
    private TextView prioGroupField;
    private Button confirm;
    private IVaxService vaxService;
    private PandemicApp pandemicApp;
    private TextView id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_appointment);

        this.pandemicApp = (PandemicApp) getApplication();

        this.prioGroupField = findViewById(R.id.priorityTxt);
        this.id = findViewById(R.id.idTxt);
        this.confirm = findViewById(R.id.confirmBtn);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer num = Integer.valueOf(prioGroupField.getText().toString());
                Integer ide = Integer.valueOf(id.getText().toString());
                triggerRestClient(num, ide);
            }
        });
    }
    //Adds the User to the Waitlist
    public void triggerRestClient(Integer prioGroup, Integer id){
        this.vaxService = RetroFitConnection.init();

        Call<WaitListDTO> waitListDTOCall = this.vaxService.addToWaitlist("Bearer " + this.pandemicApp.getJwt(), prioGroup, id);
        WaitListDTO waitListDTO = new WaitListDTO(prioGroup);
        waitListDTOCall.enqueue( new Callback<WaitListDTO>() {
            @Override
            public void onResponse(Call<WaitListDTO> waitListDTOCall, Response<WaitListDTO> response) {
                Toast.makeText(AddAppointmentActivity.this, "Data added to API", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder msg = new AlertDialog.Builder(AddAppointmentActivity.this);
                if(response.isSuccessful() && response.body() != null) {
                    msg.setTitle("Erfolg!");
                    msg.setMessage("Sie wurden zur Warteliste hinzugefügt.");

                } else {
                    System.out.println(response.code());
                    msg.setMessage("Hinzufügen zur Warteliste fehlgeschlagen.");
                }
            }

            @Override
            public void onFailure(Call<WaitListDTO> waitListDTOCall, Throwable exception) {
                System.out.println("Communication error occured");
                exception.printStackTrace();
            }
        });
    }

    /*@Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_appointment);
        return NavigationUI.navigateUp(navController, appBarConfiguration)
                || super.onSupportNavigateUp();
    }*/
}