package de.fhms.sweng.pandemie_app.tests;

import lombok.Getter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

//Jannik Ernst
@Getter
public class BuildConnection {

    private ITestService service;

    public BuildConnection() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://sweng-sweng-team16-api-gateway.wi-k8s.fh-muenster.de") //http://test-sweng-sweng-team16-api-gateway.wi-k8s.fh-muenster.de __ http://stu-fb09-065.de:8080 __ http://test-sweng-sweng-team16-test-microservice.wi-k8s.fh-muenster.de
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        service = retrofit.create(ITestService.class);
    }
}