package de.fhms.sweng.pandemie_app.user.data.inbound;

import java.security.Permissions;
import java.util.ArrayList;

import de.fhms.sweng.pandemie_app.shared.Permission;
import de.fhms.sweng.pandemie_app.shared.UserPermissions;
import de.fhms.sweng.pandemie_app.shared.UserRole;

// Niklas Plaß
public class UserDetailsLightTo {
    private Integer id;
    private String email;
    private String firstName;
    private String lastName;
    private UserRole role;
    private ArrayList<Permission> permissions;

    public UserDetailsLightTo() {}

    public UserDetailsLightTo(Integer id, String email, String firstName, String lastName, ArrayList<Permission> permissions, UserRole role) {
        this.id = id;
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.role = role;
        this.permissions = permissions;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserRole getRole() {
        return this.role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public ArrayList<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(ArrayList<Permission> permissions) {
        this.permissions = permissions;
    }
}
