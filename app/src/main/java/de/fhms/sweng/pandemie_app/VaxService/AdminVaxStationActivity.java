//Autor - Linus Schwinhorst
package de.fhms.sweng.pandemie_app.VaxService;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.shared.UserRole;

public class AdminVaxStationActivity extends AppCompatActivity {
    private Button addVaxStation;
    private Button updateVaxStation;
    private Button deleteVaxStation;
    private Button enableAppointments;
    private Button showAppointments;
    private Button addVaccination;
    private PandemicApp pandemicApp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_vax_station);

        this.pandemicApp = (PandemicApp) getApplication();

        //Security check for permissions
        if(this.pandemicApp.getJwt() == null || !this.pandemicApp.getUserRole().equals(UserRole.VACCINATE)){
            AlertDialog.Builder msg = new AlertDialog.Builder(AdminVaxStationActivity.this);
            msg.setTitle("No Permission");
            msg.setMessage("You have no Power here!");
            Intent i= new Intent(this, MainVaxStationActivity.class);
            startActivity(i);
        }

        this.addVaxStation = findViewById(R.id.addVaxStationBtn);
        this.updateVaxStation = findViewById(R.id.updateVaxStationBtn);
        this.deleteVaxStation = findViewById(R.id.deleteVaxStationBtn);
        this.enableAppointments = findViewById(R.id.enableAppointmentsBtn);
        this.showAppointments = findViewById(R.id.showAppointmentsBtn);

    }

    public void navigateTo(View button){
        Button btn = (Button) button;
        String tag = (String) btn.getTag();

        try {
            Intent i= new Intent(this, Class.forName(tag));
            startActivity(i);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}