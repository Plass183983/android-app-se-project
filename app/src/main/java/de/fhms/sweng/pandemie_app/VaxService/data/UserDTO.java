package de.fhms.sweng.pandemie_app.VaxService.data;

public class UserDTO {
    private String name;
    private Integer priorityGroup;
    private String address;
    private String email;
    private Boolean vaccinated;
    private String role;
    private Boolean hadAppointment;

    public UserDTO(){}

    public UserDTO(String address, Boolean hadAppointment, String name, Integer priorityGroup, String email, Boolean vaccinated, String role){
        this.address = address;
        this.name = name;
        this.priorityGroup = priorityGroup;
        this.email = email;
        this.vaccinated = vaccinated;
        this.role = role;
        this.hadAppointment = hadAppointment;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPriorityGroup() {
        return priorityGroup;
    }

    public void setPriorityGroup(Integer priorityGroup) {
        this.priorityGroup = priorityGroup;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getVaccinated() {
        return vaccinated;
    }

    public void setVaccinated(Boolean vaccinated) {
        this.vaccinated = vaccinated;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
