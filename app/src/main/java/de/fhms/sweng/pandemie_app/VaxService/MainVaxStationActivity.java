//Autor - Linus Schwinhorst
package de.fhms.sweng.pandemie_app.VaxService;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;

import de.fhms.sweng.pandemie_app.R;

public class MainVaxStationActivity extends AppCompatActivity {
    Button appointment;
    Button vaxProof;
    Button vaxStation;

    //This is the main default Activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_vax_station);

        this.appointment = findViewById(R.id.appointmentBtn);
        this.vaxProof = findViewById(R.id.vaxProofBtn);
        this.vaxStation = findViewById(R.id.vaxStationBtn);
    }

    public void navigateTo(View button){
        Button btn = (Button) button;
        String tag = (String) btn.getTag();

        try {
            Intent i= new Intent(this, Class.forName(tag));
            startActivity(i);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}