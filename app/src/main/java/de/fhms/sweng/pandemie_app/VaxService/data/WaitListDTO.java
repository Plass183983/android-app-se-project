package de.fhms.sweng.pandemie_app.VaxService.data;

import java.util.List;
import java.util.Objects;

public class WaitListDTO {
    private Integer priorityGroup;


    public WaitListDTO(){}

    public WaitListDTO(Integer priorityGroup){
        this.priorityGroup = priorityGroup;
    }

    public Integer getPriorityGroup() {
        return priorityGroup;
    }

    public void setPriorityGroup(Integer priorityGroup) {
        this.priorityGroup = priorityGroup;
    }
}