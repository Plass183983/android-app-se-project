package de.fhms.sweng.pandemie_app;

import android.app.Application;

import java.util.ArrayList;

import de.fhms.sweng.pandemie_app.shared.UserPermissions;
import de.fhms.sweng.pandemie_app.shared.UserRole;

//Niklas Plaß
public class PandemicApp extends Application {
    private String jwt;
    private UserRole userRole;
    private ArrayList<UserPermissions> userPermissions;

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public ArrayList<UserPermissions> getUserPermissions() {
        return this.userPermissions;
    }

    public void setUserPermissions(ArrayList<UserPermissions> userPermissions) {
        this.userPermissions = userPermissions;
    }
}
