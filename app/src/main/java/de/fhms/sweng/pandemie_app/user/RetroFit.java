package de.fhms.sweng.pandemie_app.user;

import de.fhms.sweng.pandemie_app.user.service.IUserService;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

// Niklas Plaß
public class RetroFit {
    //private final static String SERVICE_URL = "http://stu-fb09-038.fh-muenster.de:8080";
    private final static String SERVICE_URL = "http://sweng-sweng-team16-api-gateway.wi-k8s.fh-muenster.de";

    public static IUserService init() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(SERVICE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        return retrofit.create(IUserService.class);
    }
}
