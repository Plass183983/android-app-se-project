//Autor - Linus Schwinhorst
package de.fhms.sweng.pandemie_app.VaxService;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.VaxService.data.VaxProofDTO;
import de.fhms.sweng.pandemie_app.VaxService.service.IVaxService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VaxStationActivity extends AppCompatActivity {
    private Button showVaxStation;
    private Button addVaxStation;
    private Button editVaxStation;
    private Button deleteVaxStation;

    //Navigation Activity for VaxStation management and for getting VaxStations as User
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vax_station);

        this.showVaxStation = findViewById(R.id.showVaxStationBtn);
        this.addVaxStation = findViewById(R.id.addVaxStationBtn);
        this.editVaxStation = findViewById(R.id.editVaxStationBtn);
        this.deleteVaxStation = findViewById(R.id.deleteVaxStationBtn);
    }

    public void navigateTo(View button){
        Button btn = (Button) button;
        String tag = (String) btn.getTag();

        try {
            Intent i= new Intent(this, Class.forName(tag));
            startActivity(i);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}