package de.fhms.sweng.pandemie_app.shared;

public enum UserRole {
    ADMIN, USER, VACCINATE;
}
