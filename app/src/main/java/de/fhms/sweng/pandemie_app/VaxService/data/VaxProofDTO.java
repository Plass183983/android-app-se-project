package de.fhms.sweng.pandemie_app.VaxService.data;

import java.util.Date;

public class VaxProofDTO {
    private Integer id;
    private Date date;
    //private VaccinationEntity vaccinationEntity;
    //private UserEntity userEntity;

    public VaxProofDTO(){}

    public VaxProofDTO(Date date){
        this.date = date;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }
}