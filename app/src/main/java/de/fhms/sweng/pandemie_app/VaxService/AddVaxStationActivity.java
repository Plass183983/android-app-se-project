//Autor - Linus Schwinhorst
package de.fhms.sweng.pandemie_app.VaxService;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.VaxService.data.VaxStationDTO;
import de.fhms.sweng.pandemie_app.VaxService.data.WaitListDTO;
import de.fhms.sweng.pandemie_app.VaxService.service.IVaxService;
import de.fhms.sweng.pandemie_app.shared.UserRole;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddVaxStationActivity extends AppCompatActivity {
    private TextView userName;
    private TextView stationName;
    private TextView address;
    private TextView phoneNumber;
    private TextView email;
    private IVaxService vaxService;
    private Button confirm;
    private PandemicApp pandemicApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vax_station);

        this.pandemicApp = (PandemicApp) getApplication();

        //Security checks for permissions
        if(this.pandemicApp.getJwt() == null || !this.pandemicApp.getUserRole().equals(UserRole.VACCINATE)){
            AlertDialog.Builder msg = new AlertDialog.Builder(AddVaxStationActivity.this);
            msg.setTitle("No Permission");
            msg.setMessage("You have no Power here!");
            Intent i= new Intent(this, MainVaxStationActivity.class);
            startActivity(i);
        }

        this.userName = findViewById(R.id.userNameTxt);
        this.stationName = findViewById(R.id.vaxStationNameTxt);
        this.address = findViewById(R.id.addressTxt);
        this.phoneNumber = findViewById(R.id.phoneNumberTxt);
        this.email = findViewById(R.id.emailTxt);
        this.confirm = findViewById(R.id.confirmBtn);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = userName.getText().toString();
                String sName = stationName.getText().toString();
                String adr = address.getText().toString();
                String phone = phoneNumber.getText().toString();
                String mail = email.getText().toString();

                triggerRestClient(name, sName, adr, phone, mail);
            }
        });
    }

    //Adds a VaxStation to the database
    public void triggerRestClient(String userName, String stationName, String address, String phoneNumber, String email){
        this.vaxService = RetroFitConnection.init();

        Call<VaxStationDTO> vaxStationDTOCall = this.vaxService.addVaxStation("Bearer " + this.pandemicApp.getJwt(), userName, stationName, address, phoneNumber, email);
        vaxStationDTOCall.enqueue(new Callback<VaxStationDTO>() {
            @Override
            public void onResponse(Call<VaxStationDTO> vaxStationDTOCall, Response<VaxStationDTO> response) {
                AlertDialog.Builder msg = new AlertDialog.Builder(AddVaxStationActivity.this);
                if(response.isSuccessful() && response.body() != null) {
                    msg.setTitle("Erfolg!");
                    msg.setMessage("Sie haben die Impfstation hinzugefügt.");
                } else {
                    msg.setTitle("Error");
                    msg.setMessage("Hinzufügen der Impfstation fehlgeschlagen.");
                }
            }

            @Override
            public void onFailure(Call<VaxStationDTO> vaxStationDTOCall, Throwable exception) {
                System.out.println("Communication error occured");
            }
        });
    }
}