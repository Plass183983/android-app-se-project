package de.fhms.sweng.pandemie_app.VaxService.data;

import java.util.Date;

public class AppointmentDTO {
    private Date date;
    private Boolean appeared;
    //private UserEntity userEntity;
    //private VaccinationEntity vaccinationEntity;

    public AppointmentDTO(){}

    public AppointmentDTO(Boolean appeared, Date date){
        this.appeared = appeared;
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Boolean getAppeared() {
        return appeared;
    }

    public void setAppeared(Boolean appeared) {
        this.appeared = appeared;
    }
}