//Autor - Linus Schwinhorst
package de.fhms.sweng.pandemie_app.VaxService;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.VaxService.service.IVaxService;
import de.fhms.sweng.pandemie_app.shared.UserRole;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CancelAppointmentActivity extends AppCompatActivity {
    private TextView userName;
    private IVaxService vaxService;
    private Button confirm;
    private PandemicApp pandemicApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancel_appointment);

        this.pandemicApp = (PandemicApp) getApplication();

        if(this.pandemicApp.getJwt() == null || !this.pandemicApp.getUserRole().equals(UserRole.VACCINATE)){
            AlertDialog.Builder msg = new AlertDialog.Builder(CancelAppointmentActivity.this);
            msg.setTitle("No Permission");
            msg.setMessage("You have no Power here!");
            Intent i= new Intent(this, MainVaxStationActivity.class);
            startActivity(i);
        }

        this.userName = findViewById(R.id.userNameTxt);
        this.confirm = findViewById(R.id.confirmBtn);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = userName.getText().toString();

                triggerRestClient(name);
            }
        });
    }

    //Deletes an appointment for a User
    public void triggerRestClient(String userName) {
        this.vaxService = RetroFitConnection.init();

        Call<Void> voidCall = this.vaxService.cancelAppointment("Bearer " + this.pandemicApp.getJwt() ,userName);
        voidCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> voidCall, Response<Void> response) {
                AlertDialog.Builder msg = new AlertDialog.Builder(CancelAppointmentActivity.this);
                if (response.isSuccessful() && response.body() != null) {
                    msg.setMessage("Sie haben den Termin storniert.");
                } else {
                    msg.setMessage("Stornieren des Termins fehlgeschlagen.");
                }
            }

            @Override
            public void onFailure(Call<Void> waitListDTOCall, Throwable exception) {
                System.out.println("Communication error occured");
            }
        });
    }
}