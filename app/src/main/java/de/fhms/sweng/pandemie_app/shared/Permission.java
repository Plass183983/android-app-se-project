package de.fhms.sweng.pandemie_app.shared;

public class Permission {
    private UserPermissions level;

    public Permission(UserPermissions level) {
        this.level = level;
    }

    public UserPermissions getLevel() {
        return level;
    }

    public void setLevel(UserPermissions level) {
        this.level = level;
    }
}
