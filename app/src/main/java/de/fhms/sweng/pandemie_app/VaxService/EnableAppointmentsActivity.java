//Autor - Linus Schwinhorst
package de.fhms.sweng.pandemie_app.VaxService;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Date;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.VaxService.data.WaitListDTO;
import de.fhms.sweng.pandemie_app.VaxService.service.IVaxService;
import de.fhms.sweng.pandemie_app.shared.UserRole;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EnableAppointmentsActivity extends AppCompatActivity {
    private TextView amount;
    private TextView prioGroup;
    private TextView userName;
    private IVaxService vaxService;
    private Button confirm;
    private PandemicApp pandemicApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enable_appointments);

        this.pandemicApp = (PandemicApp) getApplication();

        //Security checks for permissions
        if(this.pandemicApp.getJwt() == null || !this.pandemicApp.getUserRole().equals(UserRole.VACCINATE)){
            AlertDialog.Builder msg = new AlertDialog.Builder(EnableAppointmentsActivity.this);
            msg.setTitle("No Permission");
            msg.setMessage("You have no Power here!");
            Intent i= new Intent(this, MainVaxStationActivity.class);
            startActivity(i);
        }

        this.amount = findViewById(R.id.amountTxt);
        this.prioGroup = findViewById(R.id.priorityTxt);
        this.userName = findViewById(R.id.userNameTxt);
        this.confirm = findViewById(R.id.confirmBtn);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = userName.getText().toString();
                Integer am = Integer.valueOf(amount.getText().toString());
                Integer prio = Integer.valueOf(prioGroup.getText().toString());

                triggerRestClient(name, am, prio);
            }
        });
        }

        //Enables new appointments for priority waitlist
        public void triggerRestClient(String userName, Integer amount, Integer prioGroup) {
        this.vaxService = RetroFitConnection.init();

        Call<WaitListDTO> listDTOCall = this.vaxService.enableNewAppointments("Bearer " + this.pandemicApp.getJwt(), userName, amount, prioGroup);
        listDTOCall.enqueue(new Callback<WaitListDTO>() {
            @Override
            public void onResponse(Call<WaitListDTO> voidCall, Response<WaitListDTO> response) {
                AlertDialog.Builder msg = new AlertDialog.Builder(EnableAppointmentsActivity.this);
                if (response.isSuccessful() && response.body() != null) {
                    msg.setTitle("Erfolg!");
                    msg.setMessage("Sie neue Termine freigegeben.");
                } else {
                    msg.setTitle("Error");
                    msg.setMessage("Freigeben der Termine fehlgeschlagen.");
                }
            }

            @Override
            public void onFailure(Call<WaitListDTO> listDtoCall, Throwable exception) {
                System.out.println("Communication error occured");
            }
        });
    }
    }
