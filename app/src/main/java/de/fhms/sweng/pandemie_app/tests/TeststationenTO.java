//Jannik Ernst

package de.fhms.sweng.pandemie_app.tests;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

//Jannik Ernst
@Data
public class TeststationenTO {

    private int id;
    private String name;
    private String description;
    private int capacity;
    private AdresseTO adresse;
    private List<TermineTO> termine = new ArrayList<>();

}
