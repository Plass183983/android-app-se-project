package de.fhms.sweng.pandemie_app.Kontaktverfolgung.service;

import de.fhms.sweng.pandemie_app.Kontaktverfolgung.dto.UserTO;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

// David Frommer
public interface IContactService {

    @POST("/contact/add")
    public Call<ResponseBody> createPositiveTest(@Header("Authorization") String JWT, @Body UserTO userTO);

    @POST("/contact/phoneNumber")
    public Call<UserTO> addPhoneNumber(@Header("Authorization") String JWT, @Body UserTO userTO);

    @GET("/contact/update")
    public Call<Boolean> updateFriends(@Header("Authorization") String JWT);

}
