package de.fhms.sweng.pandemie_app.user.data;

//Niklas Plaß
public class UserLoginDataTo {
    private String email;
    private String password;

    public UserLoginDataTo() {}

    public UserLoginDataTo(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
