package de.fhms.sweng.pandemie_app.tests;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//Jannik Ernst
public class TeststationenActivity extends AppCompatActivity {

    private ListView listView;
    private FloatingActionButton floatingActionButton;
    private BuildConnection connect = new BuildConnection();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teststationen);
        listView = findViewById(R.id.list);
        floatingActionButton = findViewById(R.id.floatingActionButton4);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TeststationenActivity.this, TeststationAnlegenActivity.class);
                startActivity(intent);
            }
        });

        ITestService teststationenService = connect.getService();

        final PandemicApp pandemicApp = (PandemicApp) getApplication();
        Call<List<TeststationenTO>> call = teststationenService.listAllTeststationen("Bearer " + pandemicApp.getJwt());

        call.enqueue(new Callback<List<TeststationenTO>>() {
            @Override
            public void onResponse(Call<List<TeststationenTO>> call, Response<List<TeststationenTO>> response) {
                if (!response.isSuccessful()) {
                    if (response.code() == 403) {
                        AlertDialog.Builder msg = new AlertDialog.Builder(TeststationenActivity.this);
                        msg.setTitle("Meldung");
                        msg.setMessage("Bitte loggen Sie sich zunächst ein!");
                        msg.setCancelable(false);
                        msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        AlertDialog msgDialog = msg.create();
                        msgDialog.show();
                    };
                }
                else {
                    List<String> adapter = new ArrayList<>();
                    List<TeststationenTO> teststationenTOS = response.body();
                    Map<String, Integer> ids = new HashMap<>();
                    for (TeststationenTO s : teststationenTOS) {
                        ids.put(s.getName(), s.getId());
                        adapter.add(s.getName());
                    }

                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(TeststationenActivity.this, android.R.layout.simple_list_item_1, adapter);
                    listView.setAdapter(arrayAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            String test = (String) listView.getItemAtPosition(position);
                            Intent i = new Intent(TeststationenActivity.this, DetailActivity.class);
                            i.putExtra("id", ids.get(test));
                            startActivity(i);
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<TeststationenTO>> call, Throwable t) {

            }
        });

    }
}