package de.fhms.sweng.pandemie_app.tests;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//Jannik Ernst
public class GebuchteTermineActivity extends AppCompatActivity {

    private ListView listView;
    private BuildConnection connect = new BuildConnection();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gebuchte_termine);
        listView = findViewById(R.id.listView13);

        ITestService terminService = connect.getService();

        final PandemicApp pandemicApp = (PandemicApp) getApplication();
        Call<List<TermineTO>> call = terminService.getTermineforUser("Bearer " + pandemicApp.getJwt());

        call.enqueue(new Callback<List<TermineTO>>() {
            @Override
            public void onResponse(Call<List<TermineTO>> call, Response<List<TermineTO>> response) {
                if (!response.isSuccessful()) {
                    if (response.code() == 403) {
                        AlertDialog.Builder msg = new AlertDialog.Builder(GebuchteTermineActivity.this);
                        msg.setTitle("Meldung");
                        msg.setMessage("Bitte loggen Sie sich zunächst ein!");
                        msg.setCancelable(false);
                        msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        AlertDialog msgDialog = msg.create();
                        msgDialog.show();
                    }
                    else if (response.code() == 404) {
                        AlertDialog.Builder msg = new AlertDialog.Builder(GebuchteTermineActivity.this);
                        msg.setTitle("Meldung");
                        msg.setMessage("Keine gebuchten Termine vorhanden.");
                        msg.setCancelable(false);
                        msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        AlertDialog msgDialog = msg.create();
                        msgDialog.show();
                    }
                    else {
                        return;
                    }
                }
                else {
                    List<TermineTO> termine = response.body();
                    List<String> daten = new ArrayList<>();
                    Map<String, Integer> map = new HashMap<>();

                    for (TermineTO a : termine) {
                        daten.add("Datum: " + a.getDate() + " um " + a.getUhrzeit() + " Uhr");
                        map.put("Datum: " + a.getDate() + " um " + a.getUhrzeit() + " Uhr", a.getId());
                    }

                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(GebuchteTermineActivity.this, android.R.layout.simple_list_item_1, daten);
                    listView.setAdapter(arrayAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            AlertDialog.Builder msg = new AlertDialog.Builder(GebuchteTermineActivity.this);
                            msg.setTitle("Meldung");
                            msg.setMessage("Termin stornieren?");
                            msg.setCancelable(false);
                            msg.setPositiveButton("Stornieren", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String test = (String) listView.getItemAtPosition(position);
                                    final PandemicApp pandemicApp = (PandemicApp) getApplication();
                                    Call<Void> call2 = terminService.terminStornieren(map.get(test), "Bearer " + pandemicApp.getJwt());
                                    call2.enqueue(new Callback<Void>() {
                                        @Override
                                        public void onResponse(Call<Void> call, Response<Void> response) {
                                            if (!response.isSuccessful()) {
                                                if (response.code() == 403) {
                                                    AlertDialog.Builder msg = new AlertDialog.Builder(GebuchteTermineActivity.this);
                                                    msg.setTitle("Meldung");
                                                    msg.setMessage("Sie haben nicht die erforderlichen Berechtigungen!");
                                                    msg.setCancelable(false);
                                                    msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                                        @Override
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            finish();
                                                        }
                                                    });
                                                    AlertDialog msgDialog = msg.create();
                                                    msgDialog.show();
                                                };
                                            }
                                            else {
                                                msg.setTitle("Meldung");
                                                msg.setMessage("Termin erfolgreich storniert!");
                                                msg.setCancelable(false);
                                                msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                    @Override
                                                    public void onClick(DialogInterface dialog, int which) {
                                                        finish();
                                                    }
                                                });
                                                AlertDialog msgDialog = msg.create();
                                                msgDialog.show();
                                            }
                                        }

                                        @Override
                                        public void onFailure(Call<Void> call, Throwable t) {

                                        }
                                    });
                                }
                            });
                            msg.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            AlertDialog msgDialog = msg.create();
                            msgDialog.show();

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<TermineTO>> call, Throwable t) {

            }
        });
    }

}