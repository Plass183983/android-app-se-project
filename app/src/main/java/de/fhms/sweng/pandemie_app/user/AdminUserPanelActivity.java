package de.fhms.sweng.pandemie_app.user;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.shared.Permission;
import de.fhms.sweng.pandemie_app.shared.UserPermissions;
import de.fhms.sweng.pandemie_app.shared.UserRole;
import de.fhms.sweng.pandemie_app.user.data.inbound.UserDetailsLightTo;
import de.fhms.sweng.pandemie_app.user.service.IUserService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

// Niklas Plaß
public class AdminUserPanelActivity extends AppCompatActivity {
    private PandemicApp app;
    private TextView userEmailInput;
    private TextView errorMsg;
    private TextView foundUserEmail;
    private TextView foundUserName;
    private Switch testPermissionField;
    private Switch vaxPermissionField;
    private Switch eventPermissionField;
    private UserDetailsLightTo userDetails;
    private ProgressBar progressBar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.app = (PandemicApp) getApplication();
        setContentView(R.layout.activity_user_admin_panel);

        //Go back when not admin
        if(this.app.getJwt() == null || !this.app.getUserRole().equals(UserRole.ADMIN)){
            Intent i= new Intent(this, UserMainActivity.class);
            startActivity(i);
        }

        this.userEmailInput = findViewById(R.id.adminSearchUser);
        this.foundUserEmail = findViewById(R.id.adminFoundUserEmail);
        this.foundUserName = findViewById(R.id.adminFoundUserName);
        this.eventPermissionField = findViewById(R.id.adminUserEventPermission);
        this.testPermissionField = findViewById(R.id.adminUserTestPermission);
        this.vaxPermissionField = findViewById(R.id.adminUserVaxPermission);
        this.progressBar = findViewById(R.id.progressBarAdmin);

        this.errorMsg = findViewById(R.id.adminErrorMsg);

        this.progressBar.setVisibility(View.GONE);
    }

    public void searchUser(View button) {
        this.errorMsg.setText("");
        this.progressBar.setVisibility(View.VISIBLE);
        IUserService userService = RetroFit.init();
        String userEmail = this.userEmailInput.getText().toString();

        this.eventPermissionField.setChecked(false);
        this.testPermissionField.setChecked(false);
        this.vaxPermissionField.setChecked(false);

        Call<UserDetailsLightTo> call = userService.searchUser("Bearer " + this.app.getJwt(), userEmail);
        call.enqueue( new Callback<UserDetailsLightTo>() {
            @Override
            public void onResponse(Call<UserDetailsLightTo> call, Response<UserDetailsLightTo> response) {
                if (response.isSuccessful() && response.body() != null) {
                    UserDetailsLightTo res = response.body();
                    userDetails = res;
                    displayData(userDetails);
                } else {
                    if(response.code() == 404) errorMsg.setText("Nutzer nicht gefunden.");
                }
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<UserDetailsLightTo> call, Throwable t) {
                System.out.println("Communication error occured");
                errorMsg.setText("Ein fehler ist aufgetreten! Versuche es später erneut.");
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void displayData(UserDetailsLightTo userDetails) {
        this.foundUserEmail.setText(userDetails.getEmail());
        this.foundUserName.setText(userDetails.getFirstName() + " " + userDetails.getLastName());
        for(Permission p: userDetails.getPermissions()){
            if(p.getLevel().equals(UserPermissions.EVENT)) this.eventPermissionField.setChecked(true);
            if(p.getLevel().equals(UserPermissions.TEST)) this.testPermissionField.setChecked(true);
            if(p.getLevel().equals(UserPermissions.VACCINATE)) this.vaxPermissionField.setChecked(true);
        }
    }

    public void saveData(View button) {
        this.errorMsg.setText("");
        IUserService userService = RetroFit.init();
        ArrayList<Permission> permissions = new ArrayList<>();
        this.progressBar.setVisibility(View.VISIBLE);

        if(this.eventPermissionField.isChecked()) permissions.add(new Permission(UserPermissions.EVENT));
        if(this.vaxPermissionField.isChecked()) permissions.add(new Permission(UserPermissions.VACCINATE));
        if(this.testPermissionField.isChecked()) permissions.add(new Permission(UserPermissions.TEST));

        this.userDetails.setPermissions(permissions);
        Call<ResponseBody> call = userService.updateUserData(this.userDetails.getId(), "Bearer " + this.app.getJwt(), permissions);
        call.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful() && response.body() != null) {
                    new AlertDialog.Builder(AdminUserPanelActivity.this)
                            .setTitle("Update erfolgreich")
                            .setMessage("das Profil wurde erfolgreich aktualisiert")
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                } else {
                    if(response.code() == 406) {
                        errorMsg.setText("Dieser User ist ein Admin und kann keine weiteren Berechtigungen erhalten.");
                    } else {
                        errorMsg.setText("Etwas ist schief gegangen! " + response.message());
                    }
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("Communication error occured");
                errorMsg.setText("Ein fehler ist aufgetreten! Versuche es später erneut.");
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void deleteUser(View Button) {
        new AlertDialog.Builder(AdminUserPanelActivity.this)
                .setTitle("User wirklich löschen?")
                .setMessage("alle Daten dieses Nutzers werden unwiderruflich gelöscht!")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        if(userDetails != null) {
                            deleteUser();
                        } else {
                            errorMsg.setText("Bitte wähle vorher einen Benutzer.");
                        }
                    }
                })
                .show();
    }

    public void deleteUser() {
        this.errorMsg.setText("");
        IUserService userService = RetroFit.init();
        this.progressBar.setVisibility(View.VISIBLE);

        Call<ResponseBody> call = userService.deleteUser("Bearer " + this.app.getJwt(), this.userDetails.getId());
        call.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful() && response.body() != null) {
                    new AlertDialog.Builder(AdminUserPanelActivity.this)
                            .setTitle("User gelöscht!")
                            .setMessage("dieses Profil wurde erfolgreich gelöscht")
                            .setPositiveButton(android.R.string.ok, null)
                            .show();
                } else {
                    errorMsg.setText("Etwas ist schief gegangen! " + response.message());
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("Communication error occured");
                errorMsg.setText("Ein fehler ist aufgetreten! Versuche es später erneut.");
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void goBack(View button) {
        Intent intent = new Intent(getApplicationContext(), UserMainActivity.class);
        startActivity(intent);
    }

}
