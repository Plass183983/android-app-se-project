package de.fhms.sweng.pandemie_app.tests;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//Jannik Ernst
public class TeststationAendernActivity extends AppCompatActivity {

    EditText name;
    EditText beschreibung;
    EditText kapazitaet;
    EditText strasse;
    EditText hausnr;
    EditText plz;
    EditText city;
    Button senden;
    BuildConnection connect = new BuildConnection();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teststation_aendern);
        name = findViewById(R.id.editTextTextPersonName);
        beschreibung = findViewById(R.id.editTextTextPersonName4);
        kapazitaet = findViewById(R.id.editTextTextPersonName9);
        strasse = findViewById(R.id.editTextTextPersonName6);
        hausnr = findViewById(R.id.editTextTextPersonName11);
        plz = findViewById(R.id.editTextTextPersonName12);
        city = findViewById(R.id.editTextTextPersonName8);
        senden = findViewById(R.id.button6);

        final PandemicApp pandemicApp = (PandemicApp) getApplication();
        Call<TeststationenTO> vorEinsetzen = connect.getService().getTeststationById(getIntent().getIntExtra("id", 0), "Bearer " + pandemicApp.getJwt());
        vorEinsetzen.enqueue(new Callback<TeststationenTO>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(Call<TeststationenTO> call, Response<TeststationenTO> response) {
                TeststationenTO teststationenTO = response.body();
                name.append(teststationenTO.getName());
                beschreibung.append(teststationenTO.getDescription());
                kapazitaet.append(Integer.toString(teststationenTO.getCapacity()));
                strasse.append(teststationenTO.getAdresse().getStrasse());
                hausnr.append(teststationenTO.getAdresse().getHausnr());
                plz.append(Integer.toString(teststationenTO.getAdresse().getPlz()));
                city.append(teststationenTO.getAdresse().getCity());
            }

            @Override
            public void onFailure(Call<TeststationenTO> call, Throwable t) {

            }
        });

        senden.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PandemicApp pandemicApp = (PandemicApp) getApplication();
                Call<TeststationenTO> einsetzen = connect.getService().updateTeststationen(getIntent().getIntExtra("id", 0), name.getText().toString(), beschreibung.getText().toString(), Integer.parseInt(kapazitaet.getText().toString()), strasse.getText().toString(), hausnr.getText().toString(), Integer.parseInt(plz.getText().toString()), city.getText().toString(), "Bearer " + pandemicApp.getJwt());
                einsetzen.enqueue(new Callback<TeststationenTO>() {
                    @Override
                    public void onResponse(Call<TeststationenTO> call, Response<TeststationenTO> response) {
                        if (!response.isSuccessful()) {
                            if (response.code() == 403) {
                                AlertDialog.Builder msg = new AlertDialog.Builder(TeststationAendernActivity.this);
                                msg.setTitle("Meldung");
                                msg.setMessage("Sie haben nicht die erforderlichen Berechtigungen!");
                                msg.setCancelable(false);
                                msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });
                                AlertDialog msgDialog = msg.create();
                                msgDialog.show();
                            };
                        }
                        else {
                            AlertDialog.Builder msg = new AlertDialog.Builder(TeststationAendernActivity.this);
                            msg.setTitle("Meldung");
                            msg.setMessage("Änderungen erfolgreich gespeichert!");
                            msg.setCancelable(false);
                            msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(TeststationAendernActivity.this, TeststationenActivity.class);
                                    finish();
                                    startActivity(intent);
                                }
                            });
                            msg.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            AlertDialog msgDialog = msg.create();
                            msgDialog.show();
                        }
                    }

                    @Override
                    public void onFailure(Call<TeststationenTO> call, Throwable t) {

                    }
                });
            }
        }));
    }
}