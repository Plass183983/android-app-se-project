//Jannik Ernst

package de.fhms.sweng.pandemie_app.tests;

import lombok.Data;

//Jannik Ernst
@Data
public class AdresseTO {
    String strasse;
    String hausnr;
    int plz;
    String city;
}
