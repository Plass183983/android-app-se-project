package de.fhms.sweng.pandemie_app.tests;

import lombok.AllArgsConstructor;
import lombok.Data;

//Jannik Ernst
@Data
@AllArgsConstructor
public class TeststationenDetails {
    private int id;
    private String name;
    private String description;
    private int capacity;
    private String strasse;
    private String hausnr;
    private int plz;
    private String city;

}

