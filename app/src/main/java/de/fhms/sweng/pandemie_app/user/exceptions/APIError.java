package de.fhms.sweng.pandemie_app.user.exceptions;

//Niklas Plaß
public class APIError {
    private int statusCode;
    private String message;

    public APIError() {
    }

    public int status() {
        return statusCode;
    }

    public String message() {
        return message;
    }
}
