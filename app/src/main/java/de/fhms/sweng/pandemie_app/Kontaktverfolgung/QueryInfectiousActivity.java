package de.fhms.sweng.pandemie_app.Kontaktverfolgung;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.logging.Logger;

import de.fhms.sweng.pandemie_app.Kontaktverfolgung.dto.UserTO;
import de.fhms.sweng.pandemie_app.Kontaktverfolgung.service.IContactService;
import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.databinding.ActivityKontaktverfolgungCreatePostiveTestBinding;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

// David Frommer
public class QueryInfectiousActivity extends AppCompatActivity {

    private final static String apiGateway = "http://sweng-sweng-team16-api-gateway.wi-k8s.fh-muenster.de";
    private final static String VM = "http://stu-fb09-255.fh-muenster.de:8080";
    private IContactService iContactService;
    Button button1;
    ImageView warningView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kontaktverfolgung_query_infectious);

        this.button1 = findViewById(R.id.buttonqueryinfectious1);
        this.warningView = findViewById(R.id.imageView4);

        this.warningView.setVisibility(View.INVISIBLE);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiGateway) //API-Gateway
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.iContactService = retrofit.create(IContactService.class);


        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button1.setEnabled(true);
                updateFriends();
            }
        });

    }

    protected void updateFriends() {
        showToast("Bekannte werden geprüft");
        PandemicApp app = (PandemicApp) getApplication();
        Call<Boolean> call = this.iContactService.updateFriends("Bearer " + app.getJwt());
        call.enqueue(new Callback<Boolean>() {

            @Override
            public void onResponse(Call<Boolean> call, Response<Boolean> response) {
                if (response.isSuccessful()) {
                    Boolean positiveFriends = response.body();
                    if (positiveFriends == true) {
                        warningView.setVisibility(View.VISIBLE);
                        warningView.setBackgroundColor(Color.RED);
                        showToast("Bekannte sind infektiös");
                    }
                    else {
                        warningView.setVisibility(View.VISIBLE);
                        warningView.setBackgroundColor(Color.GREEN);
                        showToast("Bekannte sind nicht infektiös");
                    }
                }
                else{
                    showToast("create test failed");
                }
            }

            @Override
            public void onFailure(Call<Boolean> call, Throwable t) {
                System.out.println("Communication error occured.");
                System.out.println(t.getMessage());
            }

        });
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.show();
    }

}
