package de.fhms.sweng.pandemie_app.Kontaktverfolgung;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

import java.util.logging.Logger;

import de.fhms.sweng.pandemie_app.Kontaktverfolgung.dto.UserTO;
import de.fhms.sweng.pandemie_app.Kontaktverfolgung.service.IContactService;
import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.databinding.ActivityKontaktverfolgungCreatePostiveTestBinding;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

// David Frommer
public class CreatePositiveTestActivity extends AppCompatActivity {

    private final static String apiGateway = "http://sweng-sweng-team16-api-gateway.wi-k8s.fh-muenster.de";
    private final static String VM = "http://stu-fb09-255.fh-muenster.de:8080";
    private IContactService iContactService;
    private ActivityKontaktverfolgungCreatePostiveTestBinding binding;
    //TextView userEmail;
    TextView covidDate;
    TextView phoneNumber;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kontaktverfolgung_create_postive_test);

        Button button1;

        //this.userEmail = findViewById(R.id.editTextTextEmailAddress);
        this.covidDate = findViewById(R.id.textDatePostive);
        this.phoneNumber = findViewById(R.id.textDatePostive);

        button1 = findViewById(R.id.button5);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiGateway)  //API-Gateway
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.iContactService = retrofit.create(IContactService.class);


        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button1.setEnabled(false);
                createPositiveTest();
            }
        });
    }


    protected void sendData(UserTO userTO) {
        PandemicApp app = (PandemicApp) getApplication();
        Call<ResponseBody> call = this.iContactService.createPositiveTest("Bearer " + app.getJwt(), userTO);

        call.enqueue(new Callback<ResponseBody>() {

            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    showToast("create test was successful");
                }
                else{
                    showToast("create test failed");
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("Communication error occured.");
                System.out.println(t.getMessage());
            }

        });

    }

    protected void createPositiveTest() {
        String covidDate = this.covidDate.getText().toString();
        //String userEmail = this.userEmail.getText().toString();
        String phoneNumber = this.phoneNumber.getText().toString();
        showToast("Date: " + covidDate);
        UserTO userTO = new UserTO("", "", "", phoneNumber, false, covidDate);
        this.sendData(userTO);
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.show();
    }


}
