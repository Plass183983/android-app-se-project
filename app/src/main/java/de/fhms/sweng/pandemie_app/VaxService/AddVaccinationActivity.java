//Autor - Linus Schwinhorst
package de.fhms.sweng.pandemie_app.VaxService;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.VaxService.data.VaxProofDTO;
import de.fhms.sweng.pandemie_app.VaxService.service.IVaxService;
import de.fhms.sweng.pandemie_app.shared.UserRole;
import de.fhms.sweng.pandemie_app.user.UserMainActivity;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddVaccinationActivity extends AppCompatActivity {
    private TextView userName;
    private Button confirm;
    private IVaxService vaxService;
    private PandemicApp pandemicApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vaccination);

        this.pandemicApp = (PandemicApp) getApplication();

        //Security permission checks
        if(this.pandemicApp.getJwt() == null || !this.pandemicApp.getUserRole().equals(UserRole.VACCINATE)){
            AlertDialog.Builder msg = new AlertDialog.Builder(AddVaccinationActivity.this);
            msg.setTitle("No Permission");
            msg.setMessage("You have no Power here!");
            Intent i= new Intent(this, MainVaxStationActivity.class);
            startActivity(i);
        }

        this.userName = findViewById(R.id.userNameTxt);
        this.confirm = findViewById(R.id.confirmBtn);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = userName.getText().toString();

                triggerRestClient(name);
            }
        });
    }

    //Add a Vaccination for a User
    public void triggerRestClient(String name) {
        this.vaxService = RetroFitConnection.init();

        Call<VaxProofDTO> vaxProofDTOCall = this.vaxService.addVaccination("Bearer " + this.pandemicApp.getJwt(), name );
        vaxProofDTOCall.enqueue(new Callback<VaxProofDTO>() {
            @Override
            public void onResponse(Call<VaxProofDTO> vaxProofDTOCall, Response<VaxProofDTO> response) {
                AlertDialog.Builder msg = new AlertDialog.Builder(AddVaccinationActivity.this);
                if (response.isSuccessful() && response.body() != null) {
                    msg.setTitle("Erfolg!");
                    msg.setMessage("Die Impfung wurde hinzugefügt.");
                } else {
                    msg.setTitle("Error");
                    msg.setMessage("Hinzufügen der Impfung fehlgeschlagen.");
                }
            }

            @Override
            public void onFailure(Call<VaxProofDTO> vaxProofDTOCall, Throwable exception) {
                System.out.println("Communication error occured");
            }
        });
    }
}