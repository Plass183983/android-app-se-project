package de.fhms.sweng.pandemie_app.tests;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//Jannik Ernst
public class TeststationAnlegenActivity extends AppCompatActivity {

    EditText name;
    EditText beschreibung;
    EditText kapazitaet;
    EditText strasse;
    EditText hausnr;
    EditText plz;
    EditText city;
    Button senden;
    BuildConnection connect = new BuildConnection();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_teststation_anlegen);
        name = findViewById(R.id.editTextTextPersonName);
        beschreibung = findViewById(R.id.editTextTextPersonName4);
        kapazitaet = findViewById(R.id.editTextTextPersonName5);
        strasse = findViewById(R.id.editTextTextPersonName6);
        hausnr = findViewById(R.id.editTextNumber);
        plz = findViewById(R.id.editTextNumber2);
        city = findViewById(R.id.editTextTextPersonName8);
        senden = findViewById(R.id.button6);

        senden.setOnClickListener((new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final PandemicApp pandemicApp = (PandemicApp) getApplication();
                Call<TeststationenTO> einsetzen = connect.getService().createNewTeststation(name.getText().toString(), beschreibung.getText().toString(), Integer.parseInt(kapazitaet.getText().toString()), strasse.getText().toString(), hausnr.getText().toString(), Integer.parseInt(plz.getText().toString()), city.getText().toString(), "Bearer " + pandemicApp.getJwt());
                einsetzen.enqueue(new Callback<TeststationenTO>() {
                    @Override
                    public void onResponse(Call<TeststationenTO> call, Response<TeststationenTO> response) {
                        if (!response.isSuccessful()) {
                            if (response.code() == 403) {
                                AlertDialog.Builder msg = new AlertDialog.Builder(TeststationAnlegenActivity.this);
                                msg.setTitle("Meldung");
                                msg.setMessage("Sie haben nicht die erforderlichen Berechtigungen!");
                                msg.setCancelable(false);
                                msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        finish();
                                    }
                                });
                                AlertDialog msgDialog = msg.create();
                                msgDialog.show();
                            };
                        }
                        else {
                            AlertDialog.Builder msg = new AlertDialog.Builder(TeststationAnlegenActivity.this);
                            msg.setTitle("Meldung");
                            msg.setMessage("Teststation erfolgreich angelegt!");
                            msg.setCancelable(false);
                            msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(TeststationAnlegenActivity.this, TeststationenActivity.class);
                                    finish();
                                    startActivity(intent);
                                }
                            });
                            msg.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            AlertDialog msgDialog = msg.create();
                            msgDialog.show();
                        }
                    }

                    @Override
                    public void onFailure(Call<TeststationenTO> call, Throwable t) {

                    }
                });
            }
        }));
    }
}