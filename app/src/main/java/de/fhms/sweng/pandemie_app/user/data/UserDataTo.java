package de.fhms.sweng.pandemie_app.user.data;

import java.util.ArrayList;

import de.fhms.sweng.pandemie_app.shared.Permission;
import de.fhms.sweng.pandemie_app.shared.UserRole;

//Niklas Plaß
public class UserDataTo {
    private String firstName;
    private String lastName;
    private String email;
    private String phone;
    private String password;
    private String passwordRepeat;
    private UserAddressTo address;
    private UserRole role;
    private ArrayList<Permission> permissions;

    public UserDataTo() {}

    public UserDataTo(String firstName, String lastName, String email, String phone, String password, String passwordRepeat, UserAddressTo address, UserRole role, ArrayList<Permission> permissions) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.passwordRepeat = passwordRepeat;
        this.address = address;
        this.role = role;
        this.permissions = permissions;
    }

    public UserDataTo(String firstName, String lastName, String email, String phone, String password, String passwordRepeat, UserAddressTo address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.password = password;
        this.passwordRepeat = passwordRepeat;
        this.address = address;
    }

    public UserDataTo(String firstName, String lastName, String email, String phone, UserAddressTo address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordRepeat() {
        return passwordRepeat;
    }

    public void setPasswordRepeat(String passwordRepeat) {
        this.passwordRepeat = passwordRepeat;
    }

    public UserAddressTo getAddress() {
        return address;
    }

    public void setAddress(UserAddressTo address) {
        this.address = address;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public ArrayList<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(ArrayList<Permission> permissions) {
        this.permissions = permissions;
    }
}
