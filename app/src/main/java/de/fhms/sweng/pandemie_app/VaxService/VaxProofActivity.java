//Autor - Linus Schwinhorst
package de.fhms.sweng.pandemie_app.VaxService;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.VaxService.data.VaxProofDTO;
import de.fhms.sweng.pandemie_app.VaxService.service.IVaxService;
import de.fhms.sweng.pandemie_app.shared.UserRole;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VaxProofActivity extends AppCompatActivity {
    private TextView userName;
    private Button confirm;
    private IVaxService vaxService;
    private PandemicApp pandemicApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vax_proof);

        this.pandemicApp = (PandemicApp) getApplication();

        this.userName = findViewById(R.id.userNameTxt);
        this.confirm = findViewById(R.id.confirmBtn);

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name = userName.getText().toString();

                triggerRestClient(name);
            }
        });
    }

    //Get VaxProof for a vaccinated User
    public void triggerRestClient(String userName) {
        this.vaxService = RetroFitConnection.init();

        Call<VaxProofDTO> vaxProofDTOCall = this.vaxService.getVaxProof("Bearer " + this.pandemicApp.getJwt(), userName);
        vaxProofDTOCall.enqueue(new Callback<VaxProofDTO>() {
            @Override
            public void onResponse(Call<VaxProofDTO> vaxProofDTOCall, Response<VaxProofDTO> response) {
                AlertDialog.Builder msg = new AlertDialog.Builder(VaxProofActivity.this);
                if (response.isSuccessful() && response.body() != null) {
                    msg.setMessage("Sie haben den Impfnachweis angefordert.");
                } else {
                    msg.setMessage("Anfordern des Impfnachweises fehlgeschlagen.");
                }
            }

            @Override
            public void onFailure(Call<VaxProofDTO> vaxProofDTOCall, Throwable exception) {
                System.out.println("Communication error occured");
            }
        });
    }
}