package de.fhms.sweng.pandemie_app.shared;

public enum UserPermissions {
    TEST, VACCINATE, EVENT;
}
