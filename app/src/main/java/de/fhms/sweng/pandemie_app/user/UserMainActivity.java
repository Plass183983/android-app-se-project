package de.fhms.sweng.pandemie_app.user;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.shared.UserRole;
import de.fhms.sweng.pandemie_app.user.data.inbound.CovidStatusTo;
import de.fhms.sweng.pandemie_app.user.service.IUserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

// Niklas Plaß
public class UserMainActivity extends AppCompatActivity {
    private Button userMainAdminButton;

    private TextView userMainDate;
    private TextView userMainCountry;
    private TextView newCases;
    private TextView casesAll;
    private TextView deathsAll;
    private TextView info;
    private TextView newDeaths;
    private TextView newCasesCompared;
    private IUserService userService;
    private ProgressBar progressBar;
    private PandemicApp pandemicApp;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_main);

        this.userMainDate = findViewById(R.id.userMainDate);
        this.userMainCountry = findViewById(R.id.userMainCountry);
        this.newCases = findViewById(R.id.newCasesMain);
        this.newCasesCompared = findViewById(R.id.mainNewCasesCompared);
        this.casesAll = findViewById(R.id.allCases);
        this.deathsAll = findViewById(R.id.allDeaths);
        this.info = findViewById(R.id.mainInfo);
        this.newDeaths = findViewById(R.id.newDeaths);
        this.progressBar = findViewById(R.id.progressBarMain);
        this.info.setVisibility(View.GONE);
        this.pandemicApp = (PandemicApp) getApplication();

        if(pandemicApp.getJwt() == null) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        }

        this.fetchData(pandemicApp.getJwt(), pandemicApp.getUserRole());
        this.userMainAdminButton = findViewById(R.id.userMainAdminButton);
    }

    public void fetchData(String jwt, UserRole role) {
        this.userService = RetroFit.init();

        Call<CovidStatusTo> call = this.userService.fetchCovidStatus( "Bearer " + jwt);
        call.enqueue( new Callback<CovidStatusTo>() {
            @Override
            public void onResponse(Call<CovidStatusTo> call, Response<CovidStatusTo> response) {
                if (response.isSuccessful() && response.body() != null) {
                    CovidStatusTo res = response.body();

                    if(res.getNewConfirmed() == 0) {
                        info.setVisibility(View.VISIBLE);
                    }

                    if(role.equals(UserRole.ADMIN)) {
                        userMainAdminButton.setVisibility(View.VISIBLE);
                    }

                    progressBar.setVisibility(View.GONE);
                    displayData(res);
                }
            }

            @Override
            public void onFailure(Call<CovidStatusTo> call, Throwable t) {
                System.out.println("Communication error occurred");
            }
        });
    }

    public void displayData(CovidStatusTo status) {
        this.userMainCountry.setText(status.getCountry());
        this.userMainDate.setText(status.getDate());
        this.newCases.setText(status.getNewConfirmed().toString());

        if(Integer.valueOf(status.getDifferenceNewConfirmed()) < 0){
            this.newCasesCompared.setTextColor(Color.GREEN);
            this.newCasesCompared.setText(status.getDifferenceNewConfirmed() + " zum Vortag");
        } else {
            this.newCasesCompared.setText("+" + status.getDifferenceNewConfirmed() + " zum Vortag");
        }
        this.casesAll.setText(status.getConfirmed().toString());
        this.deathsAll.setText(status.getDeaths().toString());
        this.newDeaths.setText("+ " + status.getNewDeaths());
    }


    //Replace later
    public void navigateTo(View button){
        Button btn = (Button) button;
        String tag = (String) btn.getTag();

        try {
            Intent i= new Intent(this, Class.forName(tag));
            startActivity(i);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public void logout(View button) {
        this.pandemicApp.setJwt("");
        this.pandemicApp.setUserRole(null);

        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
    }
}
