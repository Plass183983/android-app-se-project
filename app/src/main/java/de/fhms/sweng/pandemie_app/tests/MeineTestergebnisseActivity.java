package de.fhms.sweng.pandemie_app.tests;

import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.List;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//Jannik Ernst
public class MeineTestergebnisseActivity extends AppCompatActivity {

    private ListView listView;
    private BuildConnection connect = new BuildConnection();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meine_testergebnisse);
        listView = findViewById(R.id.listView56);

        final PandemicApp pandemicApp = (PandemicApp) getApplication();
        Call<List<TermineTO>> call1 = connect.getService().getTermineWithTestsforUser("Bearer " + pandemicApp.getJwt());

        call1.enqueue(new Callback<List<TermineTO>>() {
            @Override
            public void onResponse(Call<List<TermineTO>> call, Response<List<TermineTO>> response) {
                if (!response.isSuccessful()) {
                    if (response.code() == 403) {
                        AlertDialog.Builder msg = new AlertDialog.Builder(MeineTestergebnisseActivity.this);
                        msg.setTitle("Meldung");
                        msg.setMessage("Bitte loggen Sie sich zunächst ein!");
                        msg.setCancelable(false);
                        msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        AlertDialog msgDialog = msg.create();
                        msgDialog.show();
                    }
                    else if (response.code() == 404) {
                        AlertDialog.Builder msg = new AlertDialog.Builder(MeineTestergebnisseActivity.this);
                        msg.setTitle("Meldung");
                        msg.setMessage("Keine Testergebnisse vorhanden.");
                        msg.setCancelable(false);
                        msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        AlertDialog msgDialog = msg.create();
                        msgDialog.show();
                    }
                    else {
                        return;
                    }
                }
                else {
                    List<TermineTO> termine = response.body();
                    List<String> daten = new ArrayList<>();
                    for (TermineTO t : termine) {
                        if (t.getTest() != null ) {
                            daten.add("Datum: " + t.getDate() + " um " + t.getUhrzeit() + " Uhr   Testergebnis: " + t.getTest().getStatus().toString());
                        }
                    }
                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(MeineTestergebnisseActivity.this, android.R.layout.simple_list_item_1, daten);
                    listView.setAdapter(arrayAdapter);
                }
            }

            @Override
            public void onFailure(Call<List<TermineTO>> call, Throwable t) {

            }
        });
    }
}