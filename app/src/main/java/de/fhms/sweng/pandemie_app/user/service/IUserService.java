package de.fhms.sweng.pandemie_app.user.service;

import java.util.ArrayList;

import de.fhms.sweng.pandemie_app.shared.Permission;
import de.fhms.sweng.pandemie_app.user.data.UserDataTo;
import de.fhms.sweng.pandemie_app.user.data.UserLoginDataTo;
import de.fhms.sweng.pandemie_app.user.data.inbound.AuthResponseTo;
import de.fhms.sweng.pandemie_app.user.data.inbound.CovidStatusTo;
import de.fhms.sweng.pandemie_app.user.data.inbound.UserDetailsLightTo;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

// Niklas Plaß
public interface IUserService {
    @POST("/user/login")
    public Call<AuthResponseTo> login(@Body UserLoginDataTo userData);

    @POST("/user/register")
    public Call<AuthResponseTo> register(@Body UserDataTo userData);

    @PUT("/user/{id}/update-permission")
    public Call<ResponseBody> updateUserData(@Path("id") Integer id, @Header("Authorization") String jwt, @Body ArrayList<Permission> permissions);

    @PUT("/user/update")
    public Call<UserDataTo> updateCurrentUserData(@Body UserDataTo userData, @Header("Authorization") String jwt);

    @PUT("/user/changePassword")
    public Call<AuthResponseTo> changeCurrentUserPassword(@Header("Authorization") String jwt, @Body UserDataTo userData);

    @GET("/user/covidStatus")
    public Call<CovidStatusTo> fetchCovidStatus(@Header("Authorization") String jwt);

    @GET("/user/info")
    public Call<UserDataTo> fetchCurrentUserData(@Header("Authorization") String jwt);

    @GET("/user/find")
    public Call<UserDetailsLightTo> searchUser(@Header("Authorization") String jwt, @Query("email") String email);

    @DELETE("/user/delete")
    public Call<ResponseBody> deleteCurrentUser(@Header("Authorization") String jwt);

    @DELETE("/user/{id}/delete")
    public Call<ResponseBody> deleteUser(@Header("Authorization") String jwt, @Path("id") Integer id);

}
