package de.fhms.sweng.pandemie_app.VaxService;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.VaxService.data.UserDTO;

import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.VaxService.service.IVaxService;
import de.fhms.sweng.pandemie_app.shared.UserRole;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WaitlistActivity extends AppCompatActivity {
    private Button confirm;
    private TextView id;
    private PandemicApp pandemicApp;
    private IVaxService vaxService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_waitlist);

        this.confirm = findViewById(R.id.confirmBtn);
        this.id = findViewById(R.id.userNameTxt);
        this.pandemicApp = (PandemicApp) getApplication();

        //Security permission checks
        if(this.pandemicApp.getJwt() == null || !this.pandemicApp.getUserRole().equals(UserRole.VACCINATE)){
            AlertDialog.Builder msg = new AlertDialog.Builder(WaitlistActivity.this);
            msg.setTitle("No Permission");
            msg.setMessage("You have no Power here!");
            Intent i= new Intent(this, MainVaxStationActivity.class);
            startActivity(i);
        }

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer ide = Integer.valueOf(id.getText().toString());

                triggerRestClient(ide);
            }
        });
    }

    public void triggerRestClient(Integer id){
        this.vaxService = RetroFitConnection.init();

        Call<List<UserDTO>> listCall = this.vaxService.getWaitList("Bearer " + this.pandemicApp.getJwt(), id);
        listCall.enqueue(new Callback<List<UserDTO>>() {
            @Override
            public void onResponse(Call<List<UserDTO>> listCall, Response<List<UserDTO>> response) {
                AlertDialog.Builder msg = new AlertDialog.Builder(WaitlistActivity.this);
                if (response.isSuccessful() && response.body() != null) {
                    msg.setMessage("Die Warteliste wurde erfolgreich angefragt");
                } else {
                    msg.setMessage("Anfragen der Warteliste fehlgeschlagen.");
                }
            }

            @Override
            public void onFailure(Call<List<UserDTO>> listCall, Throwable exception) {
                System.out.println("Communication error occured");
            }
        });

    }
}