package de.fhms.sweng.pandemie_app.tests;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//Jannik Ernst
public class TermineFreischaltenActivity extends AppCompatActivity {

    private ListView listView;
    private BuildConnection connect = new BuildConnection();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_termine_freischalten);
        listView = findViewById(R.id.listView12);

        ITestService terminService = connect.getService();

        final PandemicApp pandemicApp = (PandemicApp) getApplication();
        String test = pandemicApp.getJwt();
        Call<List<TermineTO>> call = terminService.getNichtFreigeschalteteTermine(getIntent().getIntExtra("id", 0), "Bearer " + pandemicApp.getJwt());

        call.enqueue(new Callback<List<TermineTO>>() {
            @Override
            public void onResponse(Call<List<TermineTO>> call, Response<List<TermineTO>> response) {
                if (!response.isSuccessful()) {
                    if (response.code() == 403) {
                        AlertDialog.Builder msg = new AlertDialog.Builder(TermineFreischaltenActivity.this);
                        msg.setTitle("Meldung");
                        msg.setMessage("Sie haben nicht die erforderlichen Berechtigungen!");
                        msg.setCancelable(false);
                        msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        AlertDialog msgDialog = msg.create();
                        msgDialog.show();
                    }
                    else if (response.code() == 404) {
                        AlertDialog.Builder msg = new AlertDialog.Builder(TermineFreischaltenActivity.this);
                        msg.setTitle("Meldung");
                        msg.setMessage("Keine Termine zum Freischalten vorhanden.");
                        msg.setCancelable(false);
                        msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                finish();
                            }
                        });
                        AlertDialog msgDialog = msg.create();
                        msgDialog.show();
                    }
                    else {
                        return;
                    }
                }
                else {
                    List<TermineTO> termine = response.body();
                    List<String> daten = new ArrayList<>();
                    Map<String, Integer> map = new HashMap<>();

                    for (TermineTO a : termine) {
                        daten.add("Datum: " + a.getDate() + " um " + a.getUhrzeit() + " Uhr");
                        map.put("Datum: " + a.getDate() + " um " + a.getUhrzeit() + " Uhr", a.getId());
                    }

                    ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(TermineFreischaltenActivity.this, android.R.layout.simple_list_item_1, daten);
                    listView.setAdapter(arrayAdapter);
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            AlertDialog.Builder msg = new AlertDialog.Builder(TermineFreischaltenActivity.this);
                            msg.setTitle("Meldung");
                            msg.setMessage("Termin freischalten?");
                            msg.setCancelable(false);
                            msg.setPositiveButton("Freischalten", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    String test = (String) listView.getItemAtPosition(position);
                                    final PandemicApp pandemicApp = (PandemicApp) getApplication();
                                    Call<Void> call2 = terminService.terminFreischalten(getIntent().getIntExtra("id", 0), map.get(test), "Bearer " + pandemicApp.getJwt());
                                    call2.enqueue(new Callback<Void>() {
                                        @Override
                                        public void onResponse(Call<Void> call, Response<Void> response) {
                                            msg.setTitle("Meldung");
                                            msg.setMessage("Termin erfolgreich freigeschaltet!");
                                            msg.setCancelable(false);
                                            msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    finish();
                                                }
                                            });
                                            AlertDialog msgDialog = msg.create();
                                            msgDialog.show();
                                        }

                                        @Override
                                        public void onFailure(Call<Void> call, Throwable t) {

                                        }
                                    });
                                }
                            });
                            msg.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            });
                            AlertDialog msgDialog = msg.create();
                            msgDialog.show();

                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<List<TermineTO>> call, Throwable t) {

            }
        });
    }
}