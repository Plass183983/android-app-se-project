package de.fhms.sweng.pandemie_app.user;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Iterator;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.shared.Permission;
import de.fhms.sweng.pandemie_app.shared.UserPermissions;
import de.fhms.sweng.pandemie_app.shared.UserRole;
import de.fhms.sweng.pandemie_app.user.data.UserAddressTo;
import de.fhms.sweng.pandemie_app.user.data.UserDataTo;
import de.fhms.sweng.pandemie_app.user.service.IUserService;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

// Niklas Plaß
public class ProfileInfoActivity extends AppCompatActivity {

    private TextView firstNameField;
    private TextView lastNameField;
    private TextView phoneField;
    private TextView emailField;
    private TextView cityField;
    private TextView countryField;
    private TextView postalField;
    private TextView streetField;
    private TextView errorField;
    private Button submitButton;
    private Button backButton;
    private IUserService userService;
    private TextView isAdminField;
    private TextView permissionsTitle;
    private ImageView permissionsTitleImage;
    private TextView userPermissionsField;
    //FrameLayout progressBar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);

        this.firstNameField = findViewById(R.id.profileFirstName);
        this.lastNameField = findViewById(R.id.profileLastName);
        this.phoneField = findViewById(R.id.profilePhone);
        this.emailField = findViewById(R.id.profileEmail);
        this.countryField = findViewById(R.id.profileCountry);
        this.postalField = findViewById(R.id.profilePostal);
        this.streetField = findViewById(R.id.profileStreet);
        this.cityField = findViewById(R.id.profileCity);
        this.errorField = findViewById(R.id.profileErrorMsg);
        this.submitButton = findViewById(R.id.profileSave);
        this.backButton = findViewById(R.id.profileBack);
        this.isAdminField = findViewById(R.id.profileIsAdmin);
        this.permissionsTitleImage = findViewById(R.id.permissionImageView);
        this.permissionsTitle = findViewById(R.id.permissionTextViewProfile);
        this.userPermissionsField = findViewById(R.id.profilePermissions);

        //UserService
        this.userService = RetroFit.init();
        this.getUserData();
    }

    protected void getUserData() {
        Toast.makeText(this, "Daten werden geladen ...", Toast.LENGTH_SHORT).show();

        errorField.setText("");
        final PandemicApp app = (PandemicApp) getApplication();

        if(app.getJwt() == null) {
            Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
            startActivity(intent);
        }

        Call<UserDataTo> call = this.userService.fetchCurrentUserData( "Bearer " + app.getJwt());
        call.enqueue( new Callback<UserDataTo>() {
            @Override
            public void onResponse(Call<UserDataTo> call, Response<UserDataTo> response) {
                if (response.isSuccessful() && response.body() != null) {
                    UserDataTo res = response.body();
                    displayData(res);
                }
            }

            @Override
            public void onFailure(Call<UserDataTo> call, Throwable t) {
                System.out.println("Communication error occurred");
            }
        });
    }

    protected void displayData(UserDataTo userData) {
        this.firstNameField.setText(userData.getFirstName());
        this.lastNameField.setText(userData.getLastName());
        this.phoneField.setText(userData.getPhone());
        this.emailField.setText(userData.getEmail());
        this.cityField.setText(userData.getAddress().getCity());
        this.postalField.setText(userData.getAddress().getPostal());
        this.streetField.setText(userData.getAddress().getStreet());
        this.countryField.setText(userData.getAddress().getCountry());

        //Admin sign
        if(UserRole.ADMIN.equals(userData.getRole())) {
            this.isAdminField.setVisibility(View.VISIBLE);
        } else {
            this.isAdminField.setVisibility(View.GONE);
        }

        if(userData.getPermissions().size() > 0) {
            String permissionsText = "";
            Iterator<Permission> permissionIterator = userData.getPermissions().iterator();

            while(permissionIterator.hasNext()) {
                permissionsText += this.determinePermissionLevel(permissionIterator.next().getLevel());
                if(permissionIterator.hasNext()) permissionsText += ",";
            }

            this.userPermissionsField.setText(permissionsText);
            this.permissionsTitleImage.setVisibility(View.VISIBLE);
            this.permissionsTitle.setVisibility(View.VISIBLE);
            this.userPermissionsField.setVisibility(View.VISIBLE);
        } else {
            this.permissionsTitleImage.setVisibility(View.GONE);
            this.permissionsTitle.setVisibility(View.GONE);
            this.userPermissionsField.setVisibility(View.GONE);
        }
    }

    private String determinePermissionLevel(UserPermissions p) {
        switch(p) {
            case TEST:
                return "Testen";
            case EVENT:
                return "Events";
            case VACCINATE:
                return "Impfen";
            default:
                return "";
        }
    }

    public void submit(View button) {
        Toast.makeText(this, "Daten werden gesandt...", Toast.LENGTH_SHORT).show();

        String firstName = this.firstNameField.getText().toString();
        String lastName = this.lastNameField.getText().toString();
        String phone = this.phoneField.getText().toString();
        String email = this.emailField.getText().toString();
        String city = this.cityField.getText().toString();
        String postal = this.postalField.getText().toString();
        String street = this.streetField.getText().toString();
        String country = this.countryField.getText().toString();

        UserAddressTo userAddressTo = new UserAddressTo(country, postal, city, street);
        UserDataTo userData = new UserDataTo(firstName, lastName, email, phone, userAddressTo );

        this.errorField.setText("");
        final PandemicApp app = (PandemicApp) getApplication();
        Call<UserDataTo> call = this.userService.updateCurrentUserData( userData, "Bearer " + app.getJwt());
        call.enqueue( new Callback<UserDataTo>() {
            @Override
            public void onResponse(Call<UserDataTo> call, Response<UserDataTo> response) {
                if (response.isSuccessful() && response.body() != null) {
                    UserDataTo res = response.body();

                    new AlertDialog.Builder(ProfileInfoActivity.this)
                            .setTitle("Update erfolgreich")
                            .setMessage("dein Profil wurde erfolgreich aktualisiert")
                            .setPositiveButton(android.R.string.ok, null)
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .show();
                    displayData(res);
                }  else {
                    errorField.setText("Bitte fülle alle Felder vollständig und korrekt aus: " + response.message());
                }
            }

            @Override
            public void onFailure(Call<UserDataTo> call, Throwable t) {
                System.out.println("Communication error occurred");
            }
        });
    }

    public void goBack(View button) {
        //Redirect to homepage
        Intent intent = new Intent(getApplicationContext(), UserMainActivity.class);
        startActivity(intent);
    }

    public void deleteData(View button) {
        new AlertDialog.Builder(ProfileInfoActivity.this)
                .setTitle("Benutzer löschen")
                .setMessage("Sind Sie sicher dass Sie ihr Profil und alle dazugehörigen Daten löschen möchten?")
                .setNegativeButton(android.R.string.no, null)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        deleteUser();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    public void deleteUser() {
        Toast.makeText(this, "Daten werden gesandt...", Toast.LENGTH_SHORT).show();
        final PandemicApp app = (PandemicApp) getApplication();
        Call<ResponseBody> call = this.userService.deleteCurrentUser( "Bearer " + app.getJwt());
        call.enqueue( new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful() && response.body() != null) {
                    app.setJwt(null);
                    app.setUserRole(null);
                    app.setUserPermissions(null);

                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(intent);
                }  else {
                    errorField.setText("Ein fehler ist aufgetreten " + response.message());
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                System.out.println("Communication error occurred");
                errorField.setText("Ein Kommunikationsfehler ist aufgetreten");
            }
        });
    }

    public void redirectPasswordChange(View button) {
        Intent intent = new Intent(getApplicationContext(), UserPasswordChangeActivity.class);
        startActivity(intent);
    }
}
