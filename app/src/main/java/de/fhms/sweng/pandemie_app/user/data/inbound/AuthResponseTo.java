package de.fhms.sweng.pandemie_app.user.data.inbound;

import java.util.ArrayList;

import de.fhms.sweng.pandemie_app.shared.Permission;
import de.fhms.sweng.pandemie_app.shared.UserRole;
//Niklas Plaß
public class AuthResponseTo {
    private String jwt;
    private UserRole role;
    private ArrayList<Permission> permissions;

    public AuthResponseTo() {}

    public AuthResponseTo(String jwt, UserRole role, ArrayList<Permission> permissions) {
        this.jwt = jwt;
        this.role = role;
        this.permissions = permissions;
    }

    public String getJwt() {
        return jwt;
    }

    public void setJwt(String jwt) {
        this.jwt = jwt;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public ArrayList<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(ArrayList<Permission> permissions) {
        this.permissions = permissions;
    }
}
