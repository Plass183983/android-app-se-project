//Autor - Linus Schwinhorst
package de.fhms.sweng.pandemie_app.VaxService;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Date;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.VaxService.data.WaitListDTO;
import de.fhms.sweng.pandemie_app.VaxService.service.IVaxService;
import de.fhms.sweng.pandemie_app.shared.UserRole;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DeleteVaxStationActivity extends AppCompatActivity {
    private TextView userName;
    private TextView id;
    private IVaxService vaxService;
    private Button confirm;
    private PandemicApp pandemicApp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_vax_station);

        this.pandemicApp = (PandemicApp) getApplication();

        //Security checks for permissions
        if(this.pandemicApp.getJwt() == null || !this.pandemicApp.getUserRole().equals(UserRole.VACCINATE)){
            AlertDialog.Builder msg = new AlertDialog.Builder(DeleteVaxStationActivity.this);
            msg.setTitle("No Permission");
            msg.setMessage("You have no Power here!");
            Intent i= new Intent(this, MainVaxStationActivity.class);
            startActivity(i);
        }

        this.userName = findViewById(R.id.userNameTxt);
        this.id = findViewById(R.id.vaxStaitonIdTxt);
        this.confirm = findViewById(R.id.confirmBtn);

        String name = userName.getText().toString();
        Integer ide = Integer.valueOf(id.getText().toString());

        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                triggerRestClient(name, ide);
            }
        });
    }

    //Deletes a VaxStation from the database
    public void triggerRestClient(String userName, Integer id) {
        this.vaxService = RetroFitConnection.init();

        Call<Void> voidCall = this.vaxService.deleteVaxStation("Bearer " + this.pandemicApp.getJwt() ,id, userName);
        voidCall.enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> voidCall, Response<Void> response) {
                AlertDialog.Builder msg = new AlertDialog.Builder(DeleteVaxStationActivity.this);
                if (response.isSuccessful() && response.body() != null) {
                    msg.setMessage("Sie haben die Impfstation gelöscht.");
                } else {
                    msg.setMessage("Löschen der Impfstation fehlgeschlagen.");
                }
            }

            @Override
            public void onFailure(Call<Void> waitListDTOCall, Throwable exception) {
                System.out.println("Communication error occured");
            }
        });
    }
}