package de.fhms.sweng.pandemie_app.tests;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//Jannik Ernst
public class DetailActivity extends AppCompatActivity {

    TextView ueberschrift;
    TextView strasse;
    TextView stadt;
    TextView beschreibung;
    TextView kapazitaet;
    Button termine;
    Button entfernen;
    Button update;
    Button termineFreischalten;
    String ueberschriftStr;
    BuildConnection connect = new BuildConnection();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ueberschrift = findViewById(R.id.textView3);
        strasse = findViewById(R.id.textView8);
        stadt = findViewById(R.id.textView9);
        beschreibung = findViewById(R.id.textView31);
        kapazitaet = findViewById(R.id.textView33);
        termine = findViewById(R.id.button);
        entfernen = findViewById((R.id.button14));
        termineFreischalten = findViewById(R.id.button5);
        update = findViewById(R.id.button13);
        AlertDialog.Builder msg = new AlertDialog.Builder(DetailActivity.this);

        ITestService teststationenService = connect.getService();
        final PandemicApp pandemicApp = (PandemicApp) getApplication();
        Call<TeststationenTO> call = teststationenService.getTeststationById(getIntent().getIntExtra("id", 0), "Bearer " + pandemicApp.getJwt());

        termine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, TermineActivity.class);
                intent.putExtra("id", getIntent().getIntExtra("id", 0));
                startActivity(intent);
            }
        });

        update.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, TeststationAendernActivity.class);
                intent.putExtra("id", getIntent().getIntExtra("id", 0));
                startActivity(intent);
            }
        });

        termineFreischalten.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, TermineFreischaltenActivity.class);
                intent.putExtra("id", getIntent().getIntExtra("id", 0));
                startActivity(intent);
            }
        });

/*        termineFreischalten.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, TermineFreischaltenActivity.class);
                intent.putExtra("id", getIntent().getIntExtra("id", 0));
                startActivity(intent);
            }
        });*/

        entfernen.setOnClickListener((new View.OnClickListener() {
            public void onClick(View v) {
                AlertDialog.Builder msg = new AlertDialog.Builder(DetailActivity.this);
                msg.setTitle("Meldung");
                msg.setMessage("Wollen Sie die " + ueberschriftStr + " wirklich löschen?");
                msg.setCancelable(false);
                final PandemicApp pandemicApp = (PandemicApp) getApplication();
                msg.setPositiveButton("Löschen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final PandemicApp pandemicApp = (PandemicApp) getApplication();
                        Call<Void> erfolg = connect.getService().deleteTeststation(getIntent().getIntExtra("id", 0), "Bearer " + pandemicApp.getJwt());
                        erfolg.enqueue(new Callback<Void>() {
                            @Override
                            public void onResponse(Call<Void> call, Response<Void> response) {
                                if (!response.isSuccessful()) {
                                    if (response.code() == 403) {
                                        AlertDialog.Builder msg = new AlertDialog.Builder(DetailActivity.this);
                                        msg.setTitle("Meldung");
                                        msg.setMessage("Sie haben nicht die erforderlichen Berechtigungen!");
                                        msg.setCancelable(false);
                                        msg.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                finish();
                                            }
                                        });
                                        AlertDialog msgDialog = msg.create();
                                        msgDialog.show();
                                    };
                                }
                                else {
                                    msg.setTitle("Meldung");
                                    msg.setMessage("Teststation erfolgreich gelöscht!");
                                    msg.setCancelable(false);
                                    msg.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            Intent intent = new Intent(DetailActivity.this, TeststationenActivity.class);
                                            intent.putExtra("id", getIntent().getIntExtra("id", 0));
                                            startActivity(intent);
                                        }
                                    });
                                    AlertDialog msgDialog = msg.create();
                                    msgDialog.show();
                                }
                            }

                            @Override
                            public void onFailure(Call<Void> call, Throwable t) {

                            }
                        });
                    }
                });
                msg.setNegativeButton("Abbrechen", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                AlertDialog msgDialog = msg.create();
                msgDialog.show();

            }
        }));

        call.enqueue(new Callback<TeststationenTO>() {
            @Override
            public void onResponse(Call<TeststationenTO> call, Response<TeststationenTO> response) {
                if (!response.isSuccessful()) {
                    //listView.setAdapter("Code: " + response.code());
                    return;
                }
                TeststationenTO b = response.body();;
                String strasseStr;
                String stadtStr;
                String beschreibungStr;
                String kapazitaetStr;

                ueberschriftStr = b.getName();
                strasseStr = b.getAdresse().getStrasse() + " " + b.getAdresse().getHausnr();
                stadtStr = b.getAdresse().getPlz() + " " + b.getAdresse().getCity();
                beschreibungStr = b.getDescription();
                kapazitaetStr = b.getCapacity() + " Personen";

                ueberschrift.append(ueberschriftStr);
                strasse.append(strasseStr);
                stadt.append(stadtStr);
                beschreibung.append(beschreibungStr);
                kapazitaet.append(kapazitaetStr);
            }

            @Override
            public void onFailure(Call<TeststationenTO> call, Throwable t) {

            }
        });
    }

}