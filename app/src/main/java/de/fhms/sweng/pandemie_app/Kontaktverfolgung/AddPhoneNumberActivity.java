package de.fhms.sweng.pandemie_app.Kontaktverfolgung;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import de.fhms.sweng.pandemie_app.Kontaktverfolgung.dto.UserTO;
import de.fhms.sweng.pandemie_app.Kontaktverfolgung.service.IContactService;
import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Body;

public class AddPhoneNumberActivity extends AppCompatActivity {

    private final static String apiGateway = "http://sweng-sweng-team16-api-gateway.wi-k8s.fh-muenster.de";
    private final static String VM = "http://stu-fb09-255.fh-muenster.de:8080";
    private IContactService iContactService;
    TextView phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kontaktverfolgung_add_phone_number);

        Button button1;

        this.phoneNumber = findViewById(R.id.phoneNumberEdit);

        button1 = findViewById(R.id.addPhoneNumberButton);

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(apiGateway)  //API-Gateway
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        this.iContactService = retrofit.create(IContactService.class);


        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                button1.setEnabled(true);
                addPhoneNumber();
                showToast("Neue Telefonnummer hinzugefügt");
            }
        });
    }


    protected void addPhoneNumber() {
        String phoneNumber = this.phoneNumber.getText().toString();
        PandemicApp app = (PandemicApp) getApplication();
        UserTO userTO = new UserTO("", "", "", phoneNumber, true, "");
        Call<UserTO> call = this.iContactService.addPhoneNumber("Bearer " + app.getJwt(), userTO);

        call.enqueue(new Callback<UserTO>() {

            @Override
            public void onResponse(Call<UserTO> call, Response<UserTO> response) {
                if (response.isSuccessful()) {
                    showToast("Neue Telefonnummer hinzugefügt");
                } else {
                    showToast("add failed");
                }
            }

            @Override
            public void onFailure(Call<UserTO> call, Throwable t) {
                System.out.println("Communication error occured.");
                System.out.println(t.getMessage());
            }

        });
    }


    private void showToast(String message) {
        Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.show();
    }


}
