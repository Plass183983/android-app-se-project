package de.fhms.sweng.pandemie_app.Kontaktverfolgung.dto;

// David Frommer
public class UserTO {

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private boolean postiveTest;
    private String email;
    private String covidDate;

    public UserTO(String firstName, String lastName, String email, String phoneNumber, boolean positiveTest, String covidDate) {
        this.covidDate = covidDate;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.postiveTest = positiveTest;
        this.phoneNumber = phoneNumber;
    }

    public String getCovidDate() {
        return covidDate;
    }

    public void setCovidDate(String covidDate) {
        this.covidDate = covidDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isPostiveTest() {
        return postiveTest;
    }

    public void setPostiveTest(boolean postiveTest) {
        this.postiveTest = postiveTest;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
