package de.fhms.sweng.pandemie_app.user;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Arrays;

import de.fhms.sweng.pandemie_app.PandemicApp;
import de.fhms.sweng.pandemie_app.R;
import de.fhms.sweng.pandemie_app.shared.Permission;
import de.fhms.sweng.pandemie_app.shared.UserPermissions;
import de.fhms.sweng.pandemie_app.shared.UserRole;
import de.fhms.sweng.pandemie_app.user.data.UserLoginDataTo;
import de.fhms.sweng.pandemie_app.user.data.inbound.AuthResponseTo;
import de.fhms.sweng.pandemie_app.user.service.IUserService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//Niklas Plaß
public class LoginActivity extends AppCompatActivity {
    private Button loginBtn;
    private TextView emailField;
    private TextView passwordField;
    private TextView errorMsg;
    private IUserService userService;
    ProgressBar progressBar;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_login);

        this.loginBtn = findViewById(R.id.loginBtn);
        this.emailField = findViewById(R.id.emailField);
        this.passwordField = findViewById(R.id.passwordField);
        this.errorMsg = findViewById(R.id.loginErrorMsg);
        this.progressBar = findViewById(R.id.progressBarRegisterLogin);
        this.progressBar.setVisibility(View.GONE);
    }

    public void login(View button) {
        // Logic to log in
        String userEmail = this.emailField.getText().toString();
        String userPassword = this.passwordField.getText().toString();

        //Send user data
        UserLoginDataTo userData = new UserLoginDataTo(userEmail, userPassword);
        this.sendData(userData);
    }

    protected void sendData(UserLoginDataTo userData) {
        errorMsg.setText("");
        this.progressBar.setVisibility(View.VISIBLE);

        this.userService = RetroFit.init();
        final PandemicApp pandemicApp = (PandemicApp) getApplication();

        Call<AuthResponseTo> call = this.userService.login(userData);
        call.enqueue( new Callback<AuthResponseTo>() {
            @Override
            public void onResponse(Call<AuthResponseTo> call, Response<AuthResponseTo> response) {
                if (response.isSuccessful() && response.body() != null) {
                    AuthResponseTo res = response.body();
                    ArrayList<UserPermissions> userPermissionsArray = new ArrayList<>();
                    pandemicApp.setJwt(res.getJwt());
                    pandemicApp.setUserRole(res.getRole());

                    // Get the user permissions and pull only the values
                    if(UserRole.USER.equals(pandemicApp.getUserRole())) {
                        for(Permission p : res.getPermissions()) {
                            userPermissionsArray.add(p.getLevel());
                        }
                    }
                    pandemicApp.setUserPermissions( userPermissionsArray );

                    //Redirect to homepage
                    Intent intent = new Intent(getApplicationContext(), UserMainActivity.class);
                    startActivity(intent);
                } else {
                    errorMsg.setText("Email oder Passwort ungültig");
                }
                progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onFailure(Call<AuthResponseTo> call, Throwable t) {
                System.out.println("Communication error occured");
                errorMsg.setText("Ein fehler ist aufgetreten! Versuche es später erneut.");
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    public void goToRegister(View button) {
        Intent intent = new Intent(getApplicationContext(), RegisterActivity.class);
        startActivity(intent);
    }
}
