package de.fhms.sweng.pandemie_app.tests;

import lombok.AllArgsConstructor;
import lombok.Data;

//Jannik Ernst
@Data
@AllArgsConstructor
public class TestsTO {
    private int id;
    private String notizen;
    private StatusTO status;
}
