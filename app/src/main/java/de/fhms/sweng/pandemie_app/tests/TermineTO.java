//Jannik Ernst

package de.fhms.sweng.pandemie_app.tests;

import lombok.AllArgsConstructor;
import lombok.Data;

//Jannik Ernst
@Data
@AllArgsConstructor
public class TermineTO {
    private int id;
    private String date;
    private String uhrzeit;
    private boolean gebucht;
    private boolean freigeschaltet;
    private String user;
    private TestsTO test;
}
