package de.fhms.sweng.pandemie_app.Kontaktverfolgung;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import de.fhms.sweng.pandemie_app.R;

public class KontaktverfolgungMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kontaktverfolung_menu);
        Button button1;
        Button button2;
        Button button3;
        button1 = findViewById(R.id.button12);
        button2 = findViewById(R.id.button11);
        button3 = findViewById(R.id.buttonbekanntschaften);

        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(KontaktverfolgungMenuActivity.this, CreatePositiveTestActivity.class);
                startActivity(intent);
            }
        });
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(KontaktverfolgungMenuActivity.this, AddPhoneNumberActivity.class);
                startActivity(intent);
            }
        });
        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(KontaktverfolgungMenuActivity.this, QueryInfectiousActivity.class);
                startActivity(intent);
            }
        });
    }
}
